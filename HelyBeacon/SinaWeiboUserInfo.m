//
//  SinaWeiboUserInfo.m
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "SinaWeiboUserInfo.h"

@implementation SinaWeiboUserInfo

- (void)updateWithDic:(NSDictionary *)dic{
    if ([dic objectForKey:@"name"] != [NSNull null]) {
        self.name = [dic objectForKey:@"name"];
    }
    if ([dic objectForKey:@"avatar_large"] != [NSNull null]) {
        self.avatar_large = [NSURL URLWithString:[dic objectForKey:@"avatar_large"]];
    }
}

@end
