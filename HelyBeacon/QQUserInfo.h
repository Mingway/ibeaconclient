//
//  QQUserInfo.h
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QQUserInfo : NSObject

@property (nonatomic, strong) NSNumber *is_lost;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSURL *figureurl;
@property (nonatomic, strong) NSURL *figureurl_1;
@property (nonatomic, strong) NSURL *figureurl_2;
@property (nonatomic, strong) NSURL *figureurl_qq_1;
@property (nonatomic, strong) NSURL *figureurl_qq_2;
@property (nonatomic, strong) NSNumber *is_yellow_vip;
@property (nonatomic, strong) NSNumber *vip;
@property (nonatomic, strong) NSNumber *yellow_vip_level;
@property (nonatomic, strong) NSNumber *level;
@property (nonatomic, strong) NSNumber *is_yellow_year_vip;

- (void)updateWithDic:(NSDictionary*)dic;

@end

