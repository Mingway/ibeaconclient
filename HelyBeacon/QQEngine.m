//
//  QQEngine.m
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "QQEngine.h"
#import "DataModel.h"
#import "CommonConstants.h"

#import "QQUserInfo.h"

@implementation QQEngine

-(id) initWithDefaultSettings {
    
    if(self = [super initWithHostName:@"graph.qq.com" customHeaderFields:nil]) {
        
    }
    return self;
}

-(void) userInfoForCompletionHandler:(QQUserInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    if ([DataModel shareInstance].QQ_AccessToken && [DataModel shareInstance].QQ_OpenId) {
        MKNetworkOperation *op = [self operationWithPath:@"user/get_user_info" params:@{@"oauth_consumer_key" : QQ_APPID , @"access_token" : [DataModel shareInstance].QQ_AccessToken , @"openid" : [DataModel shareInstance].QQ_OpenId , @"format" : @"json"} httpMethod:@"GET" ssl:YES];
        //    在请求中添加证书
        op.clientCertificate = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"client.p12"];
        op.clientCertificatePassword = @"test";
        //   当服务器端证书不合法时是否继续访问
        op.shouldContinueWithInvalidCertificate=YES;
        [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
            
            [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
                NSLog(@"%@",jsonObject);
                if ([[jsonObject objectForKey:@"ret"] intValue] == 0) {
                    QQUserInfo *userInfo = [[QQUserInfo alloc]init];
                    [userInfo updateWithDic:jsonObject];
                    responseBlock(YES,userInfo);
                }else{
                    responseBlock(NO,nil);
                }
            }];
            
        } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
            
            errorBlock(error);
        }];
        
        [self enqueueOperation:op];
    }
}

@end
