//
//  Utility.m
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "Utility.h"
#import "Shop.h"
#import "Goods.h"

@implementation Utility

+ (CGFloat)caculateCellHeightWithShopInfo:(Shop *)shop{
    CGFloat height = 70.0;
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:15.f]};
    CGSize labelMaxSize = CGSizeMake(280, 100);
    CGRect labelBounds = [shop.shopName boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    height += labelBounds.size.height;
    
    attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
    labelMaxSize = CGSizeMake(280, 100);
    labelBounds = [shop.shopAddress boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    height += (labelBounds.size.height + 5);
    
    labelMaxSize = CGSizeMake(280, 1000);
    labelBounds = [shop.shopDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    height += (labelBounds.size.height + 5);
    
    return height;
}

+ (CGFloat)caculateCellHeightWithGoodsInfo:(Goods *)goods{
    CGFloat height = 80.0;
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:15.f]};
    CGSize labelMaxSize = CGSizeMake(280, 1000);
    CGRect labelBounds = [goods.goodsName boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    height += labelBounds.size.height;
    
    attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
    labelBounds = [goods.goodsDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    height += (labelBounds.size.height + 5);
    
    height += (30 + 5);
    
    return height;
}

@end
