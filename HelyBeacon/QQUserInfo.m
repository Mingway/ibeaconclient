//
//  QQUserInfo.m
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "QQUserInfo.h"

@implementation QQUserInfo

- (void)updateWithDic:(NSDictionary *)dic{
    if ([dic objectForKey:@"is_lost"] != [NSNull null]) {
        self.is_lost = [dic objectForKey:@"is_lost"];
    }
    if ([dic objectForKey:@"nickname"] != [NSNull null]) {
        self.nickname = [dic objectForKey:@"nickname"];
    }
    if ([dic objectForKey:@"gender"] != [NSNull null]) {
        self.gender = [dic objectForKey:@"gender"];
    }
    if ([dic objectForKey:@"figureurl"] != [NSNull null]) {
        self.figureurl = [NSURL URLWithString:[dic objectForKey:@"figureurl"]];
    }
    if ([dic objectForKey:@"figureurl_1"] != [NSNull null]) {
        self.figureurl_1 = [NSURL URLWithString:[dic objectForKey:@"figureurl_1"]];
    }
    if ([dic objectForKey:@"figureurl_2"] != [NSNull null]) {
        self.figureurl_2 = [NSURL URLWithString:[dic objectForKey:@"figureurl_2"]];
    }
    if ([dic objectForKey:@"figureurl_qq_1"] != [NSNull null]) {
        self.figureurl_qq_1 = [NSURL URLWithString:[dic objectForKey:@"figureurl_qq_1"]];
    }
    if ([dic objectForKey:@"figureurl_qq_2"] != [NSNull null]) {
        self.figureurl_qq_2 = [NSURL URLWithString:[dic objectForKey:@"figureurl_qq_2"]];
    }
    if ([dic objectForKey:@"is_yellow_vip"] != [NSNull null]) {
        self.is_yellow_vip = [dic objectForKey:@"is_yellow_vip"];
    }
    if ([dic objectForKey:@"vip"] != [NSNull null]) {
        self.vip = [dic objectForKey:@"vip"];
    }
    if ([dic objectForKey:@"yellow_vip_level"] != [NSNull null]) {
        self.yellow_vip_level = [dic objectForKey:@"yellow_vip_level"];
    }
    if ([dic objectForKey:@"level"] != [NSNull null]) {
        self.level = [dic objectForKey:@"level"];
    }
    if ([dic objectForKey:@"is_yellow_year_vip"] != [NSNull null]) {
        self.is_yellow_year_vip = [dic objectForKey:@"is_yellow_year_vip"];
    }
}

@end
