//
//  MarginTextField.h
//  NewHelyData
//
//  Created by developer on 14-5-7.
//  Copyright (c) 2014年 Helydata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarginTextField : UITextField

- (void)setLeftMargin:(CGFloat)leftMargin;

@end
