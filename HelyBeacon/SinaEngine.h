//
//  SinaEngine.h
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MKNetworkEngine.h"

@class SinaWeiboUserInfo;
@interface SinaEngine : MKNetworkEngine

-(id) initWithDefaultSettings;

typedef void (^SinaUserInfoResponseBlock)(BOOL isSuccess ,SinaWeiboUserInfo *userInfo);
-(void) userInfoForCompletionHandler:(SinaUserInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

@end
