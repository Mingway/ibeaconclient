//
//  CalloutMapAnnotationView.h
//  HelyBeacon
//
//  Created by developer on 14-5-14.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <MapKit/MapKit.h>

@class Shop;
@interface CalloutMapAnnotationView : MKAnnotationView
@property (nonatomic, readonly) UIView *contentView;

- (void)setShopInfo:(Shop *)shop;

@end
