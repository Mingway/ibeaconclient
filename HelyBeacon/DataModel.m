//
//  DataModel.m
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "DataModel.h"

#define LOGINTYPE                   @"LoginType"

#define QQ_ACCESSTOKEN              @"QQ_ACCESSTOKEN"
#define QQ_OPENID                   @"QQ_OPENID"

#define SINA_ACCESSTOKEN            @"SINA_ACCESSTOKEN"
#define SINA_USERID                 @"SINA_USERID"

@implementation DataModel

+ (instancetype)shareInstance{
    static DataModel *dataModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataModel = [[DataModel alloc]init];
    });
    return dataModel;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isLogin = NO;
        
        int loginType = [[NSUserDefaults standardUserDefaults] integerForKey:LOGINTYPE];
        if (loginType == LoginTypeQQ || loginType == LoginTypeRegister || loginType == LoginTypeSinaWeibo || loginType == LoginTypeTCWeibo || loginType == LoginTypeWX) {
            self.loginType = loginType;
        }else{
            self.loginType = LoginTypeNone;
        }
        
        switch (self.loginType) {
            case LoginTypeQQ:{
                NSString *qq_accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:QQ_ACCESSTOKEN];
                NSString *qq_openId = [[NSUserDefaults standardUserDefaults] objectForKey:QQ_OPENID];
                if (qq_accessToken && qq_openId) {
                    self.QQ_AccessToken = qq_accessToken;
                    self.QQ_OpenId = qq_openId;
                }
            }
                break;
            case LoginTypeSinaWeibo:{
                NSString *sina_accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:SINA_ACCESSTOKEN];
                NSString *sina_userId = [[NSUserDefaults standardUserDefaults] objectForKey:SINA_USERID];
                if (sina_accessToken && sina_userId) {
                    self.Sina_AccessToken = sina_accessToken;
                    self.Sina_UserId = sina_userId;
                }
            }
                break;
            default:
                break;
        }
    }
    return self;
}

- (void)setQQ_AccessToken:(NSString*)accessToken openId:(NSString*)openId{
    self.QQ_AccessToken = accessToken;
    self.QQ_OpenId = openId;
    self.loginType = LoginTypeQQ;
    [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:QQ_ACCESSTOKEN];
    [[NSUserDefaults standardUserDefaults] setObject:openId forKey:QQ_OPENID];
    [[NSUserDefaults standardUserDefaults] setInteger:self.loginType forKey:LOGINTYPE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)qqLogoff{
    self.QQ_AccessToken = nil;
    self.QQ_OpenId = nil;
    self.loginType = LoginTypeNone;
    self.isLogin = NO;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:QQ_ACCESSTOKEN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:QQ_OPENID];
    [[NSUserDefaults standardUserDefaults] setInteger:self.loginType forKey:LOGINTYPE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)setSinaWeibo_AccessToken:(NSString*)accessToken userId:(NSString*)userId{
    self.Sina_AccessToken = accessToken;
    self.Sina_UserId = userId;
    self.loginType = LoginTypeSinaWeibo;
    [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:SINA_ACCESSTOKEN];
    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:SINA_USERID];
    [[NSUserDefaults standardUserDefaults] setInteger:self.loginType forKey:LOGINTYPE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)sinaLogoff{
    self.Sina_AccessToken = nil;
    self.Sina_UserId = nil;
    self.loginType = LoginTypeNone;
    self.isLogin = NO;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:SINA_ACCESSTOKEN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:SINA_USERID];
    [[NSUserDefaults standardUserDefaults] setInteger:self.loginType forKey:LOGINTYPE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
