//
//  PhoneShopDetailViewController.m
//  HelyBeacon
//
//  Created by developer on 14-4-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneShopDetailViewController.h"
#import "Shop.h"
#import "Goods.h"
#import "MBProgressHUD.h"
#import "PhoneGoodsDetailViewController.h"
#import "AppDelegate.h"
#import "MKNetworkKit.h"

@import QuartzCore;

@interface PhoneShopDetailViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>{
    MBProgressHUD *_progressHUD;
}

@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)back:(UIBarButtonItem *)sender;

@end

@implementation PhoneShopDetailViewController

+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImage"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}

-(void) dealloc {
    [self.imageLoadingOperation cancel];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:_progressHUD];
    }
    [_progressHUD show:YES];
    
    [ApplicationDelegate.helyEngine shopInfoForShopId:self.shop.shopId completionHandler:^(BOOL isSuccess, Shop *shop) {
        [_progressHUD hide:YES];
        if (isSuccess) {
            self.shop = shop;
            [self.tableView reloadData];
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"goodsDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        [[segue destinationViewController]setGoods:self.shop.goods[indexPath.item]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)back:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)call {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"是否拨打电话 %@ ？",_shop.shopTel] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
}

#pragma mark -
#pragma mark UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = 0;
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 0) {
        if (row == 0) {
            height = 99;
        }else if (row == 1){
            height = 44;
        }else if(row == 2){
            NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
            CGSize labelMaxSize = CGSizeMake(240, 100);
            CGRect labelBounds = [self.shop.shopAddress boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            height = labelBounds.size.height + 32;
        }
    }else if(section == 1){
        height = 113;
    }
    return height;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 10;
    return 1.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 3;
    }else{
        return self.shop.goods ? self.shop.goods.count : 0;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    UITableViewCell *cell;
    if (section == 0) {
        if (row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"shopCell" forIndexPath:indexPath]
            ;
            
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
            [imageView setImageFromURL:self.shop.imageUrl placeHolderImage:[UIImage imageNamed:@"placeholder"] animation:YES];
            [imageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
            imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
            imageView.clipsToBounds = YES;
            
            UILabel *nameLabel = (UILabel *)[cell viewWithTag:2];
            nameLabel.text = self.shop.shopName;
            
        }else if(row == 1){
            cell = [tableView dequeueReusableCellWithIdentifier:@"telCell" forIndexPath:indexPath]
            ;
            UILabel *telLabel = (UILabel *)[cell viewWithTag:1];
            telLabel.text = self.shop.shopTel;
            
        }else if(row == 2){
            cell = [tableView dequeueReusableCellWithIdentifier:@"addressCell" forIndexPath:indexPath]
            ;
            UILabel *addressLabel = (UILabel *)[cell viewWithTag:1];
            NSDictionary *attribute = @{NSFontAttributeName: addressLabel.font};
            CGSize labelMaxSize = CGSizeMake(240, 100);
            CGRect labelBounds = [self.shop.shopAddress boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            addressLabel.text = self.shop.shopAddress;
            [addressLabel setFrame:CGRectMake(CGRectGetMinX(addressLabel.frame), CGRectGetMinY(addressLabel.frame), labelBounds.size.width, labelBounds.size.height)];
            addressLabel.text = self.shop.shopAddress;
            
            UIImageView *imageView = (UIImageView*)[cell viewWithTag:2];
            [imageView setFrame:CGRectMake(CGRectGetMinX(imageView.frame), (labelBounds.size.height + 32 - 17)/2, CGRectGetWidth(imageView.frame), CGRectGetHeight(imageView.frame))];
        }
    }else if(section == 1){
        cell = [tableView dequeueReusableCellWithIdentifier:@"goodsCell" forIndexPath:indexPath];
        
        Goods *goodsInfo = self.shop.goods[row];
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [imageView setImageFromURL:goodsInfo.thumbnailUrl placeHolderImage:[UIImage imageNamed:@"placeholder"] animation:YES];
        [imageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
        imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        imageView.clipsToBounds = YES;
        
        UILabel *nameLabel = (UILabel *)[cell viewWithTag:2];
        nameLabel.text = goodsInfo.goodsName;
        
        UILabel *descriptionLabel = (UILabel *)[cell viewWithTag:3];
        NSDictionary *attribute = @{NSFontAttributeName: descriptionLabel.font};
        CGSize labelMaxSize = CGSizeMake(165, 42);
        CGRect labelBounds = [goodsInfo.goodsDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
        descriptionLabel.text = goodsInfo.goodsDescription;
        [descriptionLabel setFrame:CGRectMake(CGRectGetMinX(descriptionLabel.frame), CGRectGetMinY(descriptionLabel.frame), labelBounds.size.width, labelBounds.size.height)];
        
        UILabel *priceLabel = (UILabel *)[cell viewWithTag:4];
        priceLabel.text = [NSString stringWithFormat:@"%.2f",[goodsInfo.price floatValue]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 0) {
        if (row == 1) {
            [self call];
        }else if (row == 2){
            //进入地图，标记出门店位置
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_shop.shopTel]]];
            break;
        default:
            break;
    }
}

@end
