//
//  SinaWeiboUserInfo.h
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SinaWeiboUserInfo : NSObject

@property (nonatomic, strong) NSURL *avatar_large;
@property (nonatomic, strong) NSString *name;

- (void)updateWithDic:(NSDictionary *)dic;

@end



/*

 "allow_all_act_msg" = 0;
 "allow_all_comment" = 1;
 "avatar_hd" = "http://tp1.sinaimg.cn/2625267004/180/5619430744/1";
 "avatar_large" = "http://tp1.sinaimg.cn/2625267004/180/5619430744/1";
 "bi_followers_count" = 7;
 "block_app" = 0;
 "block_word" = 0;
 city = 8;
 class = 1;
 "created_at" = "Wed Dec 21 18:24:42 +0800 2011";
 description = "\U7a0b\U5e8f\U5458";
 domain = mingway1991;
 "favourites_count" = 0;
 "follow_me" = 0;
 "followers_count" = 52;
 following = 0;
 "friends_count" = 112;
 gender = m;
 "geo_enabled" = 1;
 id = 2625267004;
 idstr = 2625267004;
 lang = "zh-cn";
 location = "\U4e0a\U6d77 \U95f8\U5317\U533a";
 mbrank = 1;
 mbtype = 2;
 name = Mingway1991;
 "online_status" = 1;
 "profile_image_url" = "http://tp1.sinaimg.cn/2625267004/50/5619430744/1";
 "profile_url" = mingway1991;
 province = 31;
 ptype = 0;
 remark = "";
 
 "screen_name" = Mingway1991;
 star = 0;
 status =     {
 "attitudes_count" = 0;
 "comments_count" = 0;
 "created_at" = "Sat May 17 23:45:33 +0800 2014";
 favorited = 0;
 geo = "<null>";
 id = 3711363744698944;
 idstr = 3711363744698944;
 "in_reply_to_screen_name" = "";
 "in_reply_to_status_id" = "";
 "in_reply_to_user_id" = "";
 mid = 3711363744698944;
 mlevel = 0;
 "pic_urls" =         (
 );
 "reposts_count" = 0;
 source = "<a href=\"http://app.weibo.com/t/feed/4ACxed\" rel=\"nofollow\">iPad\U5ba2\U6237\U7aef</a>";
 text = "\U6cbb\U6807\U4e0d\U6cbb\U672c\Uff0c\U642c\U51fa\U53bb\U4e86\U9886\U5bfc\U4eba\U770b\U4e0d\U89c1\U5c31\U884c\U4e86\Uff1f";
 truncated = 0;
 visible =         {
 "list_id" = 0;
 type = 0;
 };
 };
 "statuses_count" = 318;
 url = "http://www.shimingwei.com";
 verified = 0;
 "verified_reason" = "";
 "verified_reason_url" = "";
 "verified_source" = "";
 "verified_source_url" = "";
 "verified_trade" = "";
 "verified_type" = "-1";
 weihao = "";
 "worldcup_guess" = 0;
 
*/