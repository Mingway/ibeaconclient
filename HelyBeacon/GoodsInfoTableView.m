//
//  GoodsInfoTableView.m
//  HelyBeacon
//
//  Created by developer on 14-5-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "GoodsInfoTableView.h"
#import "GoodsInfoCell.h"
#import "ShopInfoCell.h"
#import "Shop.h"
#import "Goods.h"
#import "Utility.h"

@interface GoodsInfoTableView ()<UITableViewDelegate, UITableViewDataSource>{
    Shop *_shop;
    Goods *_goods;
}

@end

@implementation GoodsInfoTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self = [super initWithFrame:frame style:style];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        self.scrollEnabled = NO;
        self.bounces = NO;
    }
    return self;
}

- (void)setShop:(Shop *)shop andGoods:(Goods *)goods{
    _shop = shop;
    _goods = goods;
    [self reloadData];
}

#pragma mark -
#pragma mark UITableViewDataSource

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
        return 10;
    return 1.0;
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 5.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [Utility caculateCellHeightWithGoodsInfo:_goods];
    }else{
        return [Utility caculateCellHeightWithShopInfo:_shop];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (_shop && _goods) {
        return 2;
    }else{
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        GoodsInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goodsInfoCell"];
        if (!cell) {
            NSArray *cells = [[NSBundle mainBundle] loadNibNamed:@"GoodsInfoCell" owner:nil options:nil];
            if (cells.count > 0) {
                cell = cells[0];
            }
            [cell setGoodsInfo:_goods];
            return cell;
        }
        return cell;
    }else if (indexPath.section == 1){
        ShopInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopInfoCell"];
        if (!cell) {
            NSArray *cells = [[NSBundle mainBundle] loadNibNamed:@"ShopInfoCell" owner:nil options:nil];
            if (cells.count > 0) {
                cell = cells[0];
            }
            [cell setShopInfo:_shop];
            return cell;
        }
        return cell;
    }else{
        UITableViewCell *cell
        = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        
        cell.textLabel.text = @"数据错误";
        
        return cell;
    }
}

@end
