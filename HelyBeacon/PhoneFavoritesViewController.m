//
//  PhoneFavoritesViewController.m
//  HelyBeacon
//
//  Created by developer on 14-4-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneFavoritesViewController.h"
#import "WaterFallFlowLayout.h"
#import "WaterFallFlowCell.h"
#import "Goods.h"
#import "MJRefresh.h"
#import "PhoneGoodsDetailViewController.h"

#define CELL_WIDTH 150
#define CELL_IDENTIFIER @"WaterfallCell"

@interface PhoneFavoritesViewController ()<UICollectionViewDataSource, WaterFallFlowLayout, MJRefreshBaseViewDelegate>{
    CGFloat _contentOffsetY;
    CGFloat _oldContentOffsetY;
    CGFloat _newContentOffsetY;
    
    MJRefreshHeaderView *_header;
}
@property (nonatomic, strong) NSMutableArray *goods;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation PhoneFavoritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.collectionView registerClass:[WaterFallFlowCell class] forCellWithReuseIdentifier:CELL_IDENTIFIER];
    [self collectionViewAddLayout];
    [self addHeaderAndFooter];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self .navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)collectionViewAddLayout{
    WaterFallFlowLayout *layout = [[WaterFallFlowLayout alloc] init];
    
    layout.sectionInset = UIEdgeInsetsMake(0, 8, 8, 8);
    layout.delegate = self;
    self.collectionView.collectionViewLayout = layout;
}

- (void)addHeaderAndFooter{
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = self.collectionView;
    header.delegate = self;
    [header beginRefreshing];
    _header = header;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self updateLayout];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
                                            duration:duration];
    [self updateLayout];
}

- (void)updateLayout{
    WaterFallFlowLayout *layout =
    (WaterFallFlowLayout *)self.collectionView.collectionViewLayout;
    layout.columnCount = self.collectionView.bounds.size.width / CELL_WIDTH;
    layout.itemWidth = CELL_WIDTH;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.goods ? self.goods.count : 0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    WaterFallFlowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    cell.clipsToBounds = YES;
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.cornerRadius = 5;
    cell.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0].CGColor;
    cell.layer.borderWidth = 1;
    [cell configCellWithGoods:self.goods[indexPath.item]];
    
    
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PhoneGoodsDetailViewController *detailViewController = [[UIApplication sharedApplication].keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"PhoneGoodsDetailViewController"];
    [detailViewController setGoods:self.goods[indexPath.item]];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - UICollectionViewWaterfallLayoutDelegate
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(WaterFallFlowLayout *)collectionViewLayout
 heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [self caculateCellHeightWithGoods:self.goods[indexPath.item]];
}

- (float)caculateCellHeightWithGoods:(Goods *)goods{
    float height = 0.0f;
    //缩略图片高度
    height += CELL_WIDTH * goods.thumbnailHeight /goods.thumbnailWidth;
    //图片和描述之间的高度
    height += 10;
    //描述高度
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.0f]};
    CGSize labelMaxSize = CGSizeMake(CELL_WIDTH - 10, 500);
    CGRect labelBounds = [goods.goodsDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    height +=  labelBounds.size.height;
    //描述与价格之间的高度
    height += 10;
    //价格高度
    height += 20;
    //价格与底部之间的高度
    height += 5;
    
    return height;
}

#pragma mark -
#pragma mark UIScrollViewDelegate
//开始拖拽视图
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _contentOffsetY = scrollView.contentOffset.y;
}

// 滚动时调用此方法(手指离开屏幕后)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    _newContentOffsetY = scrollView.contentOffset.y;
    if (_newContentOffsetY > _oldContentOffsetY && _oldContentOffsetY > _contentOffsetY) {
        // 向上滚动
    } else if (_newContentOffsetY < _oldContentOffsetY && _oldContentOffsetY < _contentOffsetY) {
        // 向下滚动
    } else {
        // 拖动
    }
    
    if (scrollView.dragging) {
        // 拖拽
        if ((scrollView.contentOffset.y - _contentOffsetY) > 5.0f) {
            // 向上拖拽
            self.tabBarController.tabBar.hidden = YES;
        } else if ((_contentOffsetY - scrollView.contentOffset.y) > 5.0f) {
            // 向下拖拽
            self.tabBarController.tabBar.hidden = NO;
        } else {
        }
    }
}

// 完成拖拽(滚动停止时调用此方法，手指离开屏幕前)
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    _oldContentOffsetY = scrollView.contentOffset.y;
}

#pragma mark -
#pragma mark MJRefreshDelegate

#pragma mark - 刷新控件的代理方法
#pragma mark 开始进入刷新状态
- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView
{
    NSLog(@"%@----开始进入刷新状态", refreshView.class);
    self.goods = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getFilePath]];
    
    [self performSelector:@selector(endRefresh) withObject:nil afterDelay:1.5f];
}

- (void)endRefresh{
    [_header endRefreshing];
    [self.collectionView reloadData];
}

- (NSString*)getFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) ;
    NSString *documentD = [paths objectAtIndex:0];
    NSString *configFile = [documentD stringByAppendingPathComponent:@"favorList.plist"];
    return configFile;
}

#pragma mark 刷新完毕
- (void)refreshViewEndRefreshing:(MJRefreshBaseView *)refreshView
{
    NSLog(@"%@----刷新完毕", refreshView.class);
}

#pragma mark 监听刷新状态的改变
- (void)refreshView:(MJRefreshBaseView *)refreshView stateChange:(MJRefreshState)state
{
    switch (state) {
        case MJRefreshStateNormal:
            NSLog(@"%@----切换到：普通状态", refreshView.class);
            break;
            
        case MJRefreshStatePulling:
            NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
            break;
            
        case MJRefreshStateRefreshing:
            NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
            break;
        default:
            break;
    }
}

- (void)dealloc
{
    NSLog(@"--dealloc---");
    [_header free];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
