//
//  HelyEngine.h
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MKNetworkEngine.h"
#import "MKNetworkKit.h"

#import "Goods.h"
#import "Shop.h"

@interface HelyEngine : MKNetworkEngine

typedef void (^HelyGoodsListResponseBlock)(BOOL isSuccess ,NSArray *goods,int totalCount);
-(void) goodsListForPageIndex:(int) pageIndex pageSize:(int)pageSize completionHandler:(HelyGoodsListResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyGoodsInfoResponseBlock)(BOOL isSuccess ,NSDictionary *goodsDic);
-(void) goodsInfoForGoodsId:(NSString *)goodsId completionHandler:(HelyGoodsInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyShopListResponseBlock)(BOOL isSuccess ,NSArray *shops);
-(void) ShopListForLatitude:(float)latitude longitude:(float)longitude completionHandler:(HelyShopListResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyShopInfoResponseBlock)(BOOL isSuccess ,Shop *shop);
-(void) shopInfoForShopId:(NSString *)shopId completionHandler:(HelyShopInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyPushResponseBlock)(BOOL isSuccess);
-(void) getPushForUUID:(NSString*)uuid major:(NSNumber*)major minor:(NSNumber*)minor completionHandler:(HelyPushResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyGoodsListForBeaconResponseBlock)(BOOL isSuccess ,NSArray *goods);
-(void) goodsListForBeaconId:(NSString*)beaconId completionHandler:(HelyGoodsListForBeaconResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

@end
