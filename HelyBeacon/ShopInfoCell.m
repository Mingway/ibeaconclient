//
//  ShopInfoCell.m
//  HelyBeacon
//
//  Created by developer on 14-5-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "ShopInfoCell.h"
#import "Shop.h"

@interface ShopInfoCell ()<UIAlertViewDelegate>{
    Shop *_shop;
}
@property (weak, nonatomic) IBOutlet UILabel *shopTelLabel;
@property (weak, nonatomic) IBOutlet UILabel *shopNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *shopAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *shopDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *telButton;
- (IBAction)call:(UIButton *)sender;

@end

@implementation ShopInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setShopInfo:(Shop *)shop{
    _shop = shop;
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:15.f]};
    CGSize labelMaxSize = CGSizeMake(280, 100);
    CGRect labelBounds = [shop.shopName boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    self.shopNameLabel.text = shop.shopName;
    [self.shopNameLabel setFrame:CGRectMake(20, 50, labelBounds.size.width, labelBounds.size.height)];
    
    attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
    labelMaxSize = CGSizeMake(280, 100);
    labelBounds = [shop.shopAddress boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    self.shopAddressLabel.text = shop.shopAddress;
    [self.shopAddressLabel setFrame:CGRectMake(20, CGRectGetMaxY(self.shopNameLabel.frame) + 10, labelBounds.size.width, labelBounds.size.height)];
    
    labelMaxSize = CGSizeMake(280, 1000);
    labelBounds = [shop.shopDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    self.shopDescriptionLabel.text = shop.shopDescription;
    [self.shopDescriptionLabel setFrame:CGRectMake(20, CGRectGetMaxY(self.shopAddressLabel.frame) + 10, labelBounds.size.width, labelBounds.size.height)];
    
    labelMaxSize = CGSizeMake(150, 21);
    attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:15.f]};
    labelBounds = [shop.shopTel boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    self.shopTelLabel.text = shop.shopTel;
    [self.shopTelLabel setFrame:CGRectMake(300 - labelBounds.size.width, CGRectGetMinY(self.shopTelLabel.frame), labelBounds.size.width, 15)];
    
    [self.telButton setFrame:CGRectMake(CGRectGetMinX(self.shopTelLabel.frame) - 45, 0, 45, 41)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)call:(UIButton *)sender {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"是否拨打电话 %@ ？",_shop.shopTel] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_shop.shopTel]]];
            break;
        default:
            break;
    }
}

@end
