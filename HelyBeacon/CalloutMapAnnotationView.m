//
//  CalloutMapAnnotationView.m
//  HelyBeacon
//
//  Created by developer on 14-5-14.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "CalloutMapAnnotationView.h"
#import "Shop.h"
#import "MKNetworkKit.h"

@import QuartzCore;

#define  Arror_height 15

@interface CalloutMapAnnotationView(){
    UIImageView *_imageView;
    Shop *_shop;
}

@property (nonatomic, readwrite) UIView *contentView;

@end


@implementation CalloutMapAnnotationView

- (id) initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
	if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        self.frame = CGRectMake(0, 0, 260, 100);
        self.centerOffset = CGPointMake(8, -70);
		self.backgroundColor = [UIColor clearColor];
	}
	return self;
}

-(void)drawInContext:(CGContextRef)context
{
    CGContextSetLineWidth(context, 2.0);
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    [self getDrawPath:context];
    CGContextFillPath(context);
    
}
- (void)getDrawPath:(CGContextRef)context
{
    CGRect rrect = self.bounds;
	CGFloat radius = 6.0;
    
	CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
	CGFloat miny = CGRectGetMinY(rrect),
    // midy = CGRectGetMidY(rrect),
    maxy = CGRectGetMaxY(rrect)-Arror_height;
    CGContextMoveToPoint(context, midx+Arror_height, maxy);
    CGContextAddLineToPoint(context,midx, maxy+Arror_height);
    CGContextAddLineToPoint(context,midx-Arror_height, maxy);
    
    CGContextAddArcToPoint(context, minx, maxy, minx, miny, radius);
    CGContextAddArcToPoint(context, minx, minx, maxx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, maxx, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextClosePath(context);
}

- (void)drawRect:(CGRect)rect {
    
    [self drawInContext:UIGraphicsGetCurrentContext()];
    
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 1.0;
    //  self.layer.shadowOffset = CGSizeMake(-5.0f, 5.0f);
    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    
    if (_shop) {
        [_shop.shopName drawWithRect:CGRectMake(120, 10, 115, 20) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:15.f]} context:nil];
        [_shop.shopAddress drawWithRect:CGRectMake(120, 33, 115, 40) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12.f]} context:nil];
    }
}


- (UIView *)contentView {
	if (!_contentView) {
		_contentView = [[UIView alloc] init];
		self.contentView.backgroundColor = [UIColor clearColor];
		self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
		[self addSubview:self.contentView];
	}
	return _contentView;
}

- (void)setShopInfo:(Shop *)shop{
    _shop = shop;
    
    if (!_imageView) {
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(8, 8, 100, 69)];
        [self.contentView addSubview:_imageView];
        [_imageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        _imageView.clipsToBounds = YES;
        _imageView.layer.borderColor = [UIColor grayColor].CGColor;
        _imageView.layer.borderWidth = 0.5;
        _imageView.layer.cornerRadius = 3;
        
        UIImageView *detailImageView = [[UIImageView alloc]initWithFrame:CGRectMake(240, 8, 14, 69)];
        detailImageView.contentMode = UIViewContentModeScaleAspectFit;
        detailImageView.image = [UIImage imageNamed:@"Arrow_Left"];
        [self.contentView addSubview:detailImageView];
    }
    
    [_imageView setImageFromURL:shop.imageUrl placeHolderImage:[UIImage imageNamed:@"placeholder"]];
    
    [self setNeedsDisplay];
}

@end
