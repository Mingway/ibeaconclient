//
//  Goods.h
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Goods : NSObject <NSCoding>
@property (nonatomic, strong) NSString *goodsId;
@property (nonatomic, strong) NSURL *thumbnailUrl;
@property (nonatomic, strong) NSString *goodsDescription;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, assign) float imageWidth;
@property (nonatomic, assign) float imageHeight;
@property (nonatomic, assign) float thumbnailWidth;
@property (nonatomic, assign) float thumbnailHeight;
@property (nonatomic, assign) int favorNum;
@property (nonatomic, strong) NSURL *imageUrl;
@property (nonatomic, strong) NSString *longDescription;
@property (nonatomic, strong) NSString *goodsName;
@property (nonatomic, strong) NSString *shopId;

- (void)updateWithDic:(NSDictionary*)dic;

@end
