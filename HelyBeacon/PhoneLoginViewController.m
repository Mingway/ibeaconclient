//
//  PhoneLoginViewController.m
//  HelyBeacon
//
//  Created by developer on 14-4-24.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneLoginViewController.h"
#import "MarginTextField.h"
#import "CommonConstants.h"
#import "DataModel.h"

#import <TencentOpenAPI/TencentOAuth.h>
#import "WeiboSDK.h"

@import QuartzCore;

@interface PhoneLoginViewController ()<UITextFieldDelegate, TencentSessionDelegate, WBHttpRequestDelegate>{
    TencentOAuth *_tencentOAuth;
}
@property (weak, nonatomic) IBOutlet MarginTextField *userNameTextField;
@property (weak, nonatomic) IBOutlet MarginTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)loginWithQQ:(UIButton *)sender;
- (IBAction)loginWithWX:(UIButton *)sender;
- (IBAction)loginWithTCWeibo:(UIButton *)sender;
- (IBAction)loginWithSinaWeibo:(UIButton *)sender;

@end

@implementation PhoneLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.userNameTextField setLeftMargin:40];
    [self.passwordTextField setLeftMargin:40];
    self.userNameTextField.layer.cornerRadius = 2;
    self.passwordTextField.layer.cornerRadius = 2;
    self.loginButton.layer.cornerRadius = 2;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureHandler:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
}

- (void)tapGestureHandler:(UITapGestureRecognizer*)tap{
    [self.userNameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginWithQQ:(UIButton *)sender {
    if (!_tencentOAuth) {
        _tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQ_APPID andDelegate:self];
    }
    [_tencentOAuth authorize:@[@"all"] inSafari:NO];
}

- (IBAction)loginWithWX:(UIButton *)sender {
}

- (IBAction)loginWithTCWeibo:(UIButton *)sender {
}

- (IBAction)loginWithSinaWeibo:(UIButton *)sender {
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = SINA_APPREDIRECTURL;
    request.scope = @"all";
    request.userInfo = @{@"SSO_From": self};
    [WeiboSDK sendRequest:request];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark TencentSessionDelegate

- (void)tencentDidLogin{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[DataModel shareInstance] setQQ_AccessToken:_tencentOAuth.accessToken openId:_tencentOAuth.openId];
    
    if ([_delegate respondsToSelector:@selector(didLoginQQ)]) {
        [_delegate didLoginQQ];
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled{
    if(cancelled){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"取消授权QQ登录" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"QQ登录失败" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (void)tencentDidNotNetWork{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"网络连接失败" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark -
#pragma mark SinaWeibo

- (void)sinaWeiboLogin{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([_delegate respondsToSelector:@selector(didLoginSinaWeibo)]) {
        [_delegate didLoginSinaWeibo];
    }
}

@end
