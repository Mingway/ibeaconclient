//
//  WaterFallFlowLayout.h
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WaterFallFlowLayout;
@protocol WaterFallFlowLayout <UICollectionViewDelegate>

@required
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(WaterFallFlowLayout *)collectionViewLayout
 heightForItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface WaterFallFlowLayout : UICollectionViewLayout
@property (nonatomic, weak) id<WaterFallFlowLayout> delegate;
@property (nonatomic, assign) NSUInteger columnCount;
@property (nonatomic, assign) CGFloat itemWidth;
@property (nonatomic, assign) UIEdgeInsets sectionInset;

@end
