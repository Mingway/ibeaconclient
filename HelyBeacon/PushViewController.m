//
//  PushViewController.m
//  HelyBeacon
//
//  Created by developer on 14-5-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PushViewController.h"
#import "Goods.h"
#import "Shop.h"
#import "MBProgressHUD.h"
#import "MDCParallaxView.h"
#import "GoodsInfoTableView.h"
#import "Utility.h"
#import "AppDelegate.h"
#import "MKNetworkKit.h"

@interface PushViewController ()<UIScrollViewDelegate>{
    MBProgressHUD *_progressHUD;
    NSArray *_goods;
    Shop *_shop;
    UIActivityViewController* _activityViewController;
    NSInteger _index;
}

@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)close:(UIBarButtonItem *)sender;
- (IBAction)share:(UIBarButtonItem *)sender;

@end

@implementation PushViewController

+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImage"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}

 
-(void) dealloc {
    [self.imageLoadingOperation cancel];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)updateUI{
    for (int i = 0; i < _goods.count; i++) {
        Goods *goodsInfo = _goods[i];
        
        CGRect backgroundRect = CGRectMake(0, 0, 320, 400);
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:backgroundRect];
        backgroundImageView.tag = 101;
        backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        GoodsInfoTableView * foregroundView = [[GoodsInfoTableView alloc]initWithFrame:CGRectMake(0, 0, 320, [Utility caculateCellHeightWithGoodsInfo:goodsInfo] + [Utility caculateCellHeightWithShopInfo:_shop] + 50) style:UITableViewStyleGrouped];
        [foregroundView setShop:_shop andGoods:goodsInfo];
        
        MDCParallaxView *parallaxView = [[MDCParallaxView alloc] initWithBackgroundView:backgroundImageView
                                                                         foregroundView:foregroundView];
        parallaxView.tag = 1000 + i;
        parallaxView.frame = CGRectMake(320 * i, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
        parallaxView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        parallaxView.backgroundHeight = 200;
        parallaxView.scrollViewDelegate = self;
        [self.scrollView addSubview:parallaxView];
    }
    [self getScrollView:self.scrollView withIndex:0];
    [self.scrollView setContentSize:CGSizeMake(320 * _goods.count, CGRectGetHeight(self.scrollView.frame))];
}

- (void)getScrollView:(UIScrollView *)scrollView withIndex:(int) index{
    _index = index;
    MDCParallaxView *parallaxView = (MDCParallaxView *)[scrollView viewWithTag:1000 + index];
    UIImageView *imageView = (UIImageView*)[parallaxView viewWithTag:101];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    Goods *goods = _goods[index];
    if (!imageView.image) {
        [imageView setImageFromURL:goods.imageUrl placeHolderImage:[UIImage imageNamed:@"placeholder"] animation:YES];
    }
}

- (void)getGoodsInfoListWithBeaconId:(NSString*)beaconId{
    
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:_progressHUD];
    }
    [_progressHUD show:YES];
    [ApplicationDelegate.helyEngine goodsListForBeaconId:beaconId completionHandler:^(BOOL isSuccess, NSArray *goods) {
        if (isSuccess) {
            _goods = goods;
            if (goods.count > 0) {
                [ApplicationDelegate.helyEngine shopInfoForShopId:[goods[0] shopId] completionHandler:^(BOOL isSuccess, Shop *shop) {
                    [_progressHUD hide:YES];
                    if (isSuccess) {
                        _shop = shop;
                        [self updateUI];
                    }else{
                        
                    }
                    
                } errorHandler:^(NSError *error) {
                    [_progressHUD hide:YES];
                }];
            }else{
               [_progressHUD hide:YES];
            }
        }else{
            [_progressHUD hide:YES];
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)share:(UIBarButtonItem *)sender {
    
    _activityViewController = nil;
    
    if (_index < _goods.count) {
        NSString *textToShare = [NSString stringWithFormat:@"%@很棒，你们也来看看吧！",[_goods[_index]  goodsName]];
        //    NSURL *urlToShare = [NSURL URLWithString:@"http://www.shimingwei.com"];
        NSArray *activityItems = @[textToShare];
        if (!_activityViewController) {
            _activityViewController = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
            _activityViewController.excludedActivityTypes = @[
                                                              UIActivityTypePrint,
                                                              UIActivityTypeCopyToPasteboard,
                                                              UIActivityTypeAssignToContact,
                                                              UIActivityTypeSaveToCameraRoll
                                                              ];
            [_activityViewController setCompletionHandler:^(NSString *act, BOOL done){
                
                NSLog(@"act type %@",act);
                NSString *ServiceMsg = nil;
                if ( [act isEqualToString:UIActivityTypeMail] )           ServiceMsg = @"邮件分享成功";
                if ( [act isEqualToString:UIActivityTypePostToTwitter] )  ServiceMsg = @"已分享到twitter";
                if ( [act isEqualToString:UIActivityTypePostToFacebook] ) ServiceMsg = @"已分享到facebook";
                if ([act isEqualToString:UIActivityTypePostToWeibo])
                    ServiceMsg = @"已分享到新浪微博";
                if ( done ){
                    UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil];
                    [Alert show];
                }
                else{
                    
                }
            }];
        }
        
        [self presentViewController:_activityViewController animated:YES completion:nil];
    }
}


#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView.tag == 10000) {
        NSInteger index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
        [self getScrollView:scrollView withIndex:index];
    }
}

@end
