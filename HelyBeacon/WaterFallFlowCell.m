//
//  WaterFallFlowCell.m
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "WaterFallFlowCell.h"
#import "Goods.h"
#import "MKNetworkKit.h"

@import QuartzCore;

@interface WaterFallFlowCell (){
    Goods *_goods;
}

@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
@property (strong, nonatomic) UIImageView *thumbnailImageView;
@property (strong, nonatomic) UILabel *descriptionLabel;
@property (strong, nonatomic) UILabel *priceLabel;
@property (strong, nonatomic) UILabel *favorNumLabel;
@property (strong, nonatomic) UIImageView *heartImageView;


@end

@implementation WaterFallFlowCell

//===========================================================
// + (BOOL)automaticallyNotifiesObserversForKey:
//
//===========================================================
+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImageView"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}

-(void) prepareForReuse {
//    self.thumbnailImageView.image = [UIImage imageNamed:@"placeholder"];
//    [self.imageLoadingOperation cancel];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
}

- (void)configCellWithGoods:(Goods *)goods
{
    if (!self.thumbnailImageView) {
        self.thumbnailImageView = [[UIImageView alloc]init];
        [self addSubview:self.thumbnailImageView];
        
        [self.thumbnailImageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
        self.thumbnailImageView.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0].CGColor;
        self.thumbnailImageView.layer.borderWidth = 1;
        self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.thumbnailImageView.clipsToBounds = YES;
    }
    if (!self.descriptionLabel) {
        self.descriptionLabel = [[UILabel alloc]init];
        self.descriptionLabel.font = [UIFont systemFontOfSize:12.f];
        self.descriptionLabel.numberOfLines = 0;
        self.descriptionLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:self.descriptionLabel];
    }
    if (!self.priceLabel) {
        self.priceLabel = [[UILabel alloc]init];
        self.priceLabel.font = [UIFont systemFontOfSize:12.f];
        self.priceLabel.textColor = [UIColor redColor];
        [self addSubview:self.priceLabel];
    }
    if (!self.favorNumLabel) {
        self.favorNumLabel = [[UILabel alloc]init];
        self.favorNumLabel.font = [UIFont systemFontOfSize:12.f];
        self.favorNumLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:self.favorNumLabel];
    }
    if (!self.heartImageView) {
        self.heartImageView = [[UIImageView alloc]init];
        [self addSubview:self.heartImageView];
        self.heartImageView.image = [UIImage imageNamed:@"heart.png"];
    }
    
    float height = CGRectGetWidth(self.bounds) * goods.thumbnailHeight /goods.thumbnailWidth;
    [self.thumbnailImageView setFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), height)];
    
    [self.thumbnailImageView setImageFromURL:goods.thumbnailUrl
                            placeHolderImage:[UIImage imageNamed:@"placeholder"]];
    
    //description Label
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
    CGSize labelMaxSize = CGSizeMake(CGRectGetWidth(self.bounds) - 10, 500);
    CGRect labelBounds = [goods.goodsDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    [self.descriptionLabel setFrame:CGRectMake(5, CGRectGetMaxY(self.
                                                                thumbnailImageView.frame) + 10, labelBounds.size.width, labelBounds.size.height)];
    self.descriptionLabel.text = goods.goodsDescription;
    
    //price Label
    [self.priceLabel setFrame:CGRectMake(5, CGRectGetMaxY(self.descriptionLabel.frame) + 10, 75, 20)];
    self.priceLabel.text = [NSString stringWithFormat:@"¥ %.2f",[goods.price floatValue]];
    
    //heart ImageView
    labelMaxSize = CGSizeMake(50, 20);
    labelBounds = [[@(goods.favorNum) stringValue] boundingRectWithSize:labelMaxSize options:NSStringDrawingTruncatesLastVisibleLine attributes:attribute context:nil];
    self.favorNumLabel.text = [@(goods.favorNum) stringValue];
    [self.favorNumLabel setFrame:CGRectMake(CGRectGetWidth(self.bounds) - labelBounds.size.width - 5, CGRectGetMaxY(self.descriptionLabel.frame) + 13, labelBounds.size.width, labelBounds.size.height)];
    [self.heartImageView setFrame:CGRectMake(CGRectGetMinX(self.favorNumLabel.frame)-18, CGRectGetMaxY(self.descriptionLabel.frame) + 13, 15, 15)];
    
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0].CGColor;
    self.layer.masksToBounds = NO;
    self.layer.contentsScale = [UIScreen mainScreen].scale;
    self.layer.borderWidth = 1;
    self.layer.shadowColor = [UIColor grayColor].CGColor;
    self.layer.shadowOpacity = 0.8f;
    self.layer.shadowRadius = 1.5f;
    self.layer.shadowOffset = CGSizeMake(-1, -1);
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
}
@end
