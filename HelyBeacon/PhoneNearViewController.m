//
//  PhoneNearViewController.m
//  HelyBeacon
//
//  Created by developer on 14-4-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneNearViewController.h"
#import "Shop.h"
#import "PhoneShopDetailViewController.h"
#import "ShopCell.h"
#import "MapViewController.h"
#import "MJRefresh.h"
#import "AppDelegate.h"

@import CoreLocation;

@interface PhoneNearViewController ()<CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, MapViewControllerDelegate, MJRefreshBaseViewDelegate>{
    MapViewController *_mapViewController;
    BOOL _isMapViewDisplayed;
    MJRefreshHeaderView *_header;
}
@property CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *shops;
- (IBAction)switchDisplayView:(UISegmentedControl *)sender;

@end

@implementation PhoneNearViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!_mapViewController) {
        _mapViewController = [[UIApplication sharedApplication].keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        _mapViewController.delegate = self;
    }
    
    if (![CLLocationManager locationServicesEnabled]){
        UIAlertView *locationServiceAlert = [[UIAlertView alloc] initWithTitle:nil message:@"定位不可用" delegate:nil cancelButtonTitle:@"我知道了" otherButtonTitles:nil];
        [locationServiceAlert show];
    }else{
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = kCLDistanceFilterNone;
    }
    
    [self addHeader];
}

- (void)addHeader{
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = self.tableView;
    header.delegate = self;
    [header beginRefreshing];
    _header = header;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.locationManager startUpdatingLocation];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.locationManager stopUpdatingLocation];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"shopDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Shop *shop = self.shops[indexPath.section];
        
        [[segue destinationViewController]setShop:shop];
    }
}

#pragma mark - Location manager delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    NSLog(@"%f,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *array, NSError *error)
     {
         if (array.count > 0){
             CLPlacemark *placemark = [array objectAtIndex:0];
             NSDictionary *addressDic = placemark.addressDictionary;
             self.locationLabel.text = [NSString stringWithFormat:@"位置：%@,%@,%@,%@,%@",addressDic[@"Country"],addressDic[@"State"],addressDic[@"SubLocality"],addressDic[@"Street"],addressDic[@"Name"]];
         }else if (error == nil && [array count] == 0){
             NSLog(@"No results were returned.");
         }else if (error != nil){
             NSLog(@"An error occurred = %@", error);
         }
     }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"location error = %@",error.description);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.shops ? self.shops.count : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ShopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopCell" forIndexPath:indexPath];
    
    Shop *shop = self.shops[indexPath.section];
    [cell configWithShop:shop];
    
    return cell;
}

- (IBAction)switchDisplayView:(UISegmentedControl *)sender {
    if (!_isMapViewDisplayed) {
        [sender setEnabled:NO forSegmentAtIndex:0];
        [UIView transitionFromView:self.tableView toView:_mapViewController.view duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft completion:^(BOOL finished){
            _isMapViewDisplayed = YES;
            [sender setEnabled:YES forSegmentAtIndex:0];
            [_mapViewController config];
        }];
    }else{
        [sender setEnabled:NO forSegmentAtIndex:1];
        [UIView transitionFromView:_mapViewController.view toView:self.tableView duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight completion:^(BOOL finished){
            _isMapViewDisplayed = NO;
           [sender setEnabled:YES forSegmentAtIndex:1];
        }];
    }
}

#pragma mark -
#pragma mark MapViewControllerDelegate

- (void)didSelectedShop:(Shop *)shop{
    UIStoryboard *storyboard = [UIApplication sharedApplication].keyWindow.rootViewController.storyboard;
    PhoneShopDetailViewController *shopDetailViewController = [storyboard instantiateViewControllerWithIdentifier:@"PhoneShopDetailViewController"];
    shopDetailViewController.shop = shop;
    [self.navigationController pushViewController:shopDetailViewController animated:YES];
}

#pragma mark -
#pragma mark MJRefreshDelegate

#pragma mark - 刷新控件的代理方法
#pragma mark 开始进入刷新状态

- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
    NSLog(@"%@----开始进入刷新状态", refreshView.class);
    
    [ApplicationDelegate.helyEngine ShopListForLatitude:0 longitude:0 completionHandler:^(BOOL isSuccess, NSArray *shops) {
        [_header endRefreshing];
        if (isSuccess) {
            self.shops = shops;
            [self.tableView reloadData];
            _mapViewController.shops = self.shops;
        }else{
            //errCode != 0
        }
    } errorHandler:^(NSError *error) {
        [_header endRefreshing];
    }];
}

#pragma mark 刷新完毕
- (void)refreshViewEndRefreshing:(MJRefreshBaseView *)refreshView{
    NSLog(@"%@----刷新完毕", refreshView.class);
}

#pragma mark 监听刷新状态的改变
- (void)refreshView:(MJRefreshBaseView *)refreshView stateChange:(MJRefreshState)state{
    switch (state) {
        case MJRefreshStateNormal:
            NSLog(@"%@----切换到：普通状态", refreshView.class);
            break;
            
        case MJRefreshStatePulling:
            NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
            break;
            
        case MJRefreshStateRefreshing:
            NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
            break;
        default:
            break;
    }
}

- (void)dealloc{
    NSLog(@"--dealloc---");
    [_header free];
}

@end
