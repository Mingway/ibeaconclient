//
//  MarginTextField.m
//  NewHelyData
//
//  Created by developer on 14-5-7.
//  Copyright (c) 2014年 Helydata. All rights reserved.
//

#import "MarginTextField.h"

@interface MarginTextField(){
    CGFloat _leftMargin;
}

@end

@implementation MarginTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    bounds.origin.x += _leftMargin;
    
    return bounds;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    bounds.origin.x += _leftMargin;
    
    return bounds;
}

- (void)setLeftMargin:(CGFloat)leftMargin{
    _leftMargin = leftMargin;
}

@end
