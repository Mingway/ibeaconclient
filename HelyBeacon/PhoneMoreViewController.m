//
//  PhoneMoreViewController.m
//  HelyBeacon
//
//  Created by developer on 14-4-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneMoreViewController.h"
#import "PhoneLoginViewController.h"
#import "DataModel.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import "CommonConstants.h"

#import "QQUserInfo.h"
#import "SinaWeiboUserInfo.h"

@interface PhoneMoreViewController ()<PhoneLoginViewControllerDelegate ,TencentSessionDelegate>{
    TencentOAuth *_tencentOAuth;
    NSFileManager *_fileMgr;
    MBProgressHUD *_progressHUD;
}
@property (weak, nonatomic) IBOutlet UILabel *cacheSizeLabel;
@property (weak, nonatomic) IBOutlet UIView *userInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *avatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
- (IBAction)logoff:(UIButton *)sender;

@end

@implementation PhoneMoreViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateCacheSize];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%i,%i",[DataModel shareInstance].isLogin,[DataModel shareInstance].loginType);
    
    if (![DataModel shareInstance].isLogin && [DataModel shareInstance].loginType == LoginTypeQQ) {
        [self getQQUserInfo];
    }
    if (![DataModel shareInstance].isLogin && [DataModel shareInstance].loginType == LoginTypeSinaWeibo) {
        [self getSinaWeiboUserInfo];
    }
}

- (void)getQQUserInfo{
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.tableView];
        [self.tableView addSubview:_progressHUD];
    }
    [_progressHUD show:YES];
    
    [ApplicationDelegate.qqEngine userInfoForCompletionHandler:^(BOOL isSuccess, QQUserInfo *userInfo) {
        [_progressHUD hide:YES];
        if (isSuccess) {
            [DataModel shareInstance].isLogin = YES;
            [DataModel shareInstance].QQ_UserInfo = userInfo;
            
            self.userInfoView.hidden = NO;
            [self.avatorImageView setImageFromURL:userInfo.figureurl_2 placeHolderImage:[UIImage imageNamed:@"avator"]];
            self.userNameLabel.text = userInfo.nickname;
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (void)getSinaWeiboUserInfo{
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.tableView];
        [self.tableView addSubview:_progressHUD];
    }
    [_progressHUD show:YES];
    
    [ApplicationDelegate.sinaEngine userInfoForCompletionHandler:^(BOOL isSuccess, SinaWeiboUserInfo *userInfo) {
        [_progressHUD hide:YES];
        if (isSuccess) {
            [DataModel shareInstance].isLogin = YES;
            [DataModel shareInstance].Sina_UserInfo = userInfo;
            
            self.userInfoView.hidden = NO;
            [self.avatorImageView setImageFromURL:userInfo.avatar_large placeHolderImage:[UIImage imageNamed:@"avator"]];
            self.userNameLabel.text = userInfo.name;
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"login"]) {
        [[segue destinationViewController] setDelegate:self];
    }
}

-(NSString*) cacheDirectoryName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *cacheDirectoryName = [documentsDirectory stringByAppendingPathComponent:@"HelyImages"];
    return cacheDirectoryName;
}

- (void)updateCacheSize{
    NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *docDir = [documentsPaths objectAtIndex:0];
    
    NSString *ch = @"Byte";
    unsigned long long cacheSize = [self folderSize:docDir] + [self folderSize:[self cacheDirectoryName]];
    unsigned long long size = cacheSize / 1000000;
    if (size > 0) {
        ch = @"MB";
    } else {
        size = cacheSize / 1000;
        if (size > 0) {
            ch = @"KB";
        } else {
            size = cacheSize;
        }
    }
    self.cacheSizeLabel.text = [NSString stringWithFormat:@"%llu%@",size,ch];
}

- (unsigned long long int)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    unsigned long long int fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        NSError *error = nil;
        NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:&error];
        fileSize += [fileDictionary fileSize];
    }
    
    return fileSize;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 1) {
        if (row == 0) {
            NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
            NSString *docDir = [documentsPaths objectAtIndex:0];
            
            if ([self clearFolder:docDir] && [self clearFolder:[self cacheDirectoryName]]) {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"清除成功" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }else{
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"清除失败" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];
            }
            [self updateCacheSize];
        }
    }else if (section == 2){
        if (row == 0) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"已经是最新版本" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

- (BOOL)clearFolder:(NSString*)folderPath{
    if (!_fileMgr) {
        _fileMgr = [[NSFileManager alloc]init];
    }
    NSError *error = nil;
    NSArray *directoryContents = [_fileMgr contentsOfDirectoryAtPath:folderPath error:&error];
    BOOL isSuccess = YES;
    if (error == nil) {
        for (NSString *path in directoryContents) {
            NSString *fullPath = [folderPath stringByAppendingPathComponent:path];
            BOOL removeSuccess = [_fileMgr removeItemAtPath:fullPath error:&error];
            if (!removeSuccess) {
                isSuccess = NO;
            }
        }
    }else {
        isSuccess = NO;
    }
    return isSuccess;
}

#pragma mark -
#pragma mark PhoneLoginViewControllerDelegate

- (void)didLoginQQ{
    //根据accessToken和openId获取用户信息
    [self getQQUserInfo];
}

- (void)didLoginSinaWeibo{
    [self getSinaWeiboUserInfo];
}

- (IBAction)logoff:(UIButton *)sender {
    if (!_tencentOAuth) {
        _tencentOAuth = [[TencentOAuth alloc]initWithAppId:QQ_APPID andDelegate:self];
    }
    [_tencentOAuth logout:self];
}

#pragma mark -
#pragma mark TencentSessionDelegate

- (void)tencentDidLogout{
    [[DataModel shareInstance] qqLogoff];
    self.userInfoView.hidden = YES;
}

@end
