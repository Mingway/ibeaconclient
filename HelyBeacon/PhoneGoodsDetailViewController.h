//
//  PhoneGoodsDetailViewController.h
//  HelyBeacon
//
//  Created by developer on 14-4-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Goods;
@interface PhoneGoodsDetailViewController : UIViewController
@property (nonatomic, strong) Goods *goods;

@end
