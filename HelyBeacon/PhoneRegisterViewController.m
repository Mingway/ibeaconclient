//
//  PhoneRegisterViewController.m
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneRegisterViewController.h"
#import "MarginTextField.h"

@import QuartzCore;

@interface PhoneRegisterViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet MarginTextField *userNameTextField;
@property (weak, nonatomic) IBOutlet MarginTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet MarginTextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

- (IBAction)cancel:(UIBarButtonItem *)sender;

@end

@implementation PhoneRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.userNameTextField setLeftMargin:40];
    [self.passwordTextField setLeftMargin:40];
    [self.phoneTextField setLeftMargin:40];
    self.userNameTextField.layer.cornerRadius = 2;
    self.passwordTextField.layer.cornerRadius = 2;
    self.phoneTextField.layer.cornerRadius = 2;
    self.registerButton.layer.cornerRadius = 2;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureHandler:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
}

- (void)tapGestureHandler:(UITapGestureRecognizer*)tap{
    [self.userNameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.phoneTextField resignFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
