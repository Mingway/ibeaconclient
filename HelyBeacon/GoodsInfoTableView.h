//
//  GoodsInfoTableView.h
//  HelyBeacon
//
//  Created by developer on 14-5-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Shop;
@class Goods;
@interface GoodsInfoTableView : UITableView

- (void)setShop:(Shop *)shop andGoods:(Goods *)goods;

@end
