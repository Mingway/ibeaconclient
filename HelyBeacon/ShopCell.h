//
//  ShopCell.h
//  HelyBeacon
//
//  Created by developer on 14-4-23.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Shop;
@interface ShopCell : UITableViewCell

- (void)configWithShop:(Shop*)shop;

@end
