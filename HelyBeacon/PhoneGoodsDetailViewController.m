//
//  PhoneGoodsDetailViewController.m
//  HelyBeacon
//
//  Created by developer on 14-4-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneGoodsDetailViewController.h"
#import "Goods.h"
#import "Shop.h"
#import "MBProgressHUD.h"
#import "MDCParallaxView.h"
#import "Utility.h"
#import "GoodsInfoTableView.h"
#import "AppDelegate.h"
#import "MKNetworkKit.h"

@import QuartzCore;

@interface PhoneGoodsDetailViewController ()<UIScrollViewDelegate>{
    MBProgressHUD *_progressHUD;
    MDCParallaxView *_parallaxView;
    Shop *_shop;
    UIActivityViewController* _activityViewController;
}

@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
- (IBAction)back:(UIBarButtonItem *)sender;
- (IBAction)share:(UIBarButtonItem *)sender;

@end

@implementation PhoneGoodsDetailViewController

+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImage"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}

-(void) dealloc {
    [self.imageLoadingOperation cancel];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getGoodsInfoListWithGoodsIdStr:self.goods.goodsId];
}

- (void)getGoodsInfoListWithGoodsIdStr:(NSString*)goodsIdStr{
    
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:_progressHUD];
    }
    [_progressHUD show:YES];
    
    [ApplicationDelegate.helyEngine goodsInfoForGoodsId:self.goods.goodsId completionHandler:^(BOOL isSuccess ,NSDictionary *goodsDic) {
        if (isSuccess) {
            [self.goods updateWithDic:goodsDic];
            
            [ApplicationDelegate.helyEngine shopInfoForShopId:self.goods.shopId completionHandler:^(BOOL isSuccess, Shop *shop) {
                [_progressHUD hide:YES];
                _shop = shop;
                [self updateUI];
            } errorHandler:^(NSError *error) {
                [_progressHUD hide:YES];
            }];
        }else{
            [_progressHUD hide:YES];
            //errCode != 0 or not get data
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (void)updateUI{
    CGRect backgroundRect = CGRectMake(0, 0, 320, 400);
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:backgroundRect];
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    [backgroundImageView setImageFromURL:self.goods.imageUrl placeHolderImage:[UIImage imageNamed:@"placeholder"] animation:YES];
    
    GoodsInfoTableView * foregroundView = [[GoodsInfoTableView alloc]initWithFrame:CGRectMake(0, 0, 320, [Utility caculateCellHeightWithGoodsInfo:self.goods] + [Utility caculateCellHeightWithShopInfo:_shop] + 50) style:UITableViewStyleGrouped];
    [foregroundView setShop:_shop andGoods:self.goods];
    
    MDCParallaxView *parallaxView = [[MDCParallaxView alloc] initWithBackgroundView:backgroundImageView
                                                                     foregroundView:foregroundView];
    parallaxView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    parallaxView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    parallaxView.backgroundHeight = 200;
    parallaxView.scrollViewDelegate = self;
    [self.view addSubview:parallaxView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)back:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)share:(UIBarButtonItem *)sender {
    _activityViewController = nil;
    
    NSString *textToShare = [NSString stringWithFormat:@"%@很棒，你们也来看看吧！",self.goods.goodsName];
    //    NSURL *urlToShare = [NSURL URLWithString:@"http://www.shimingwei.com"];
    NSArray *activityItems = @[textToShare];
    if (!_activityViewController) {
        _activityViewController = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
        _activityViewController.excludedActivityTypes = @[
                                                          UIActivityTypePrint,
                                                          UIActivityTypeCopyToPasteboard,
                                                          UIActivityTypeAssignToContact,
                                                          UIActivityTypeSaveToCameraRoll
                                                          ];
        [_activityViewController setCompletionHandler:^(NSString *act, BOOL done){
            
            NSLog(@"act type %@",act);
            NSString *ServiceMsg = nil;
            if ( [act isEqualToString:UIActivityTypeMail] )           ServiceMsg = @"邮件分享成功";
            if ( [act isEqualToString:UIActivityTypePostToTwitter] )  ServiceMsg = @"已分享到twitter";
            if ( [act isEqualToString:UIActivityTypePostToFacebook] ) ServiceMsg = @"已分享到facebook";
            if ([act isEqualToString:UIActivityTypePostToWeibo])
                ServiceMsg = @"已分享到新浪微博";
            if ( done ){
                UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil];
                [Alert show];
            }
            else{
                
            }
        }];
    }
    
    [self presentViewController:_activityViewController animated:YES completion:nil];
}

@end
