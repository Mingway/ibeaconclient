//
//  MapViewController.h
//  HelyBeacon
//
//  Created by developer on 14-5-7.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Shop;
@protocol MapViewControllerDelegate <NSObject>

@required
- (void)didSelectedShop:(Shop*)shop;

@end

@interface MapViewController : UIViewController
@property (nonatomic, strong) NSArray *shops;
@property (nonatomic) id<MapViewControllerDelegate> delegate;
- (void)config;
@end
