//
//  GoodsInfoCell.h
//  HelyBeacon
//
//  Created by developer on 14-5-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Goods;
@interface GoodsInfoCell : UITableViewCell

- (void)setGoodsInfo:(Goods *)goods;

@end
