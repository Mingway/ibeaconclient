//
//  QQEngine.h
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MKNetworkEngine.h"

@class QQUserInfo;

@interface QQEngine : MKNetworkEngine

-(id) initWithDefaultSettings;

typedef void (^QQUserInfoResponseBlock)(BOOL isSuccess ,QQUserInfo *userInfo);
-(void) userInfoForCompletionHandler:(QQUserInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

@end
