//
//  WaterFallFlowCell.h
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Goods;
@interface WaterFallFlowCell : UICollectionViewCell
- (void)configCellWithGoods:(Goods *)goods;
@end
