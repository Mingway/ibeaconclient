//
//  AppDelegate.m
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "AppDelegate.h"
#import "PushViewController.h"

#import "MKNetworkKit.h"
#import <TencentOpenAPI/TencentOAuth.h>

#import "WeiboSDK.h"
#import "CommonConstants.h"

#import "DataModel.h"

#import "PhoneLoginViewController.h"

@import CoreLocation;

@interface AppDelegate ()<CLLocationManagerDelegate , WeiboSDKDelegate>
@property CLLocationManager *locationManager;
@property CLLocationManager *locationManager2;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.helyEngine = [[HelyEngine alloc] initWithHostName:@"115.29.196.234:8585"];
    [self.helyEngine useCache];
    
    [UIImageView setDefaultEngine:self.helyEngine];
    
    self.qqEngine = [[QQEngine alloc] initWithDefaultSettings];
    
    self.sinaEngine = [[SinaEngine alloc] initWithDefaultSettings];
    
    //新浪微博
    [WeiboSDK enableDebugMode:YES];
    [WeiboSDK registerApp:SINA_APPKEY];
    
    //消息推送支持的类型
    UIRemoteNotificationType types =
    (UIRemoteNotificationTypeBadge
     |UIRemoteNotificationTypeSound
     |UIRemoteNotificationTypeAlert);
    //注册消息推送
    [[UIApplication sharedApplication]registerForRemoteNotificationTypes:types];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Phone" bundle:nil]instantiateInitialViewController];
    }else{
        self.window.rootViewController = [[UIStoryboard storyboardWithName:@"Pad" bundle:nil]instantiateInitialViewController];
    }
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    NSUUID *uuid = [[NSUUID alloc]initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"];
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"com.helydata.beacon"];
    region.notifyEntryStateOnDisplay = YES;
    [self.locationManager startMonitoringForRegion:region];
    
    self.locationManager2 = [[CLLocationManager alloc] init];
    self.locationManager2.delegate = self;
    self.locationManager2.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    [self.locationManager2 startUpdatingLocation];
    
    return YES;
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{
    NSLog(@"determine");
    CLBeaconRegion *newRegion = (CLBeaconRegion*)region;
    if(state == CLRegionStateInside){
        [self.locationManager startRangingBeaconsInRegion:newRegion];
    }else if(state == CLRegionStateOutside){
        [self.locationManager stopRangingBeaconsInRegion:newRegion];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region{
    //请求推送
    NSLog(@"range");
    if (beacons.count > 0) {
        CLBeacon *minDistanceBeacon = beacons[0];
        for (int i = 1; i < beacons.count ; i++) {
            CLBeacon *beacon = beacons[i];
            if (beacon.accuracy < minDistanceBeacon.accuracy) {
                minDistanceBeacon = beacon;
            }
        }
        
        if (minDistanceBeacon.accuracy <= 10.0f) {
            NSMutableDictionary *pushListDic = [[NSMutableDictionary alloc] initWithContentsOfFile:[self getFilePath]];
            NSMutableArray *dates = [pushListDic objectForKey:[NSString stringWithFormat:@"%@+%@+%@",[minDistanceBeacon.proximityUUID UUIDString],minDistanceBeacon.major,minDistanceBeacon.minor]];
            
            if (dates) {
                NSTimeInterval  timeInterval = [[dates lastObject] timeIntervalSinceDate:[self getNowDateFromatAnDate:[NSDate date]]];
                if (timeInterval <= -60) {
                    [self getPushForBeacon:minDistanceBeacon];
                }
            }else{
                [self getPushForBeacon:minDistanceBeacon];
            }
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region{
    NSLog(@"enter");
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"exit");
}

- (void)locationManager:(CLLocationManager *)manager
rangingBeaconsDidFailForRegion:(CLBeaconRegion *)region
              withError:(NSError *)error{
    NSLog(@"error = %@",error.description);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
}

- (void)getPushForBeacon:(CLBeacon*)beacon{
    
    [self.helyEngine getPushForUUID:[beacon.proximityUUID UUIDString] major:beacon.major minor:beacon.minor completionHandler:^(BOOL isSuccess) {
        if (isSuccess) {
           
        }
    } errorHandler:^(NSError *error) {
        
    }];
    
    NSMutableDictionary *pushListDic = [[NSMutableDictionary alloc] initWithContentsOfFile:[self getFilePath]];
    if (!pushListDic) {
        pushListDic = [NSMutableDictionary dictionary];
    }
    
    NSString *key = [NSString stringWithFormat:@"%@+%@+%@",[beacon.proximityUUID UUIDString],beacon.major,beacon.minor];
    NSMutableArray *dates = [pushListDic objectForKey:key];
    if (!dates) {
        dates = [NSMutableArray array];
    }
    
    [dates addObject:[self getNowDateFromatAnDate:[NSDate date]]];
    [pushListDic setObject:dates forKey:key];
    [pushListDic writeToFile:[self getFilePath] atomically:YES];
}

- (NSString*)getFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) ;
    NSString *documentD = [paths objectAtIndex:0];
    NSString *configFile = [documentD stringByAppendingPathComponent:@"pushList.plist"];
    return configFile;
}

- (NSDate *)getNowDateFromatAnDate:(NSDate *)anyDate
{
    //设置源日期时区
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];//或GMT
    //设置转换后的目标日期时区
    NSTimeZone* destinationTimeZone = [NSTimeZone localTimeZone];
    //得到源日期与世界标准时间的偏移量
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:anyDate];
    //目标日期与本地时区的偏移量
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:anyDate];
    //得到时间偏移量的差值
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    //转为现在时间
    NSDate* destinationDateNow = [[NSDate alloc] initWithTimeInterval:interval sinceDate:anyDate];
    return destinationDateNow;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    application.applicationIconBadgeNumber = 0;
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark Push Notification

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSString* dt = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    if (dt) {
        NSLog(@"device token = %@",dt);
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:@"deviceToken"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"\napns -> didReceiveRemoteNotification,Receive Data:\n%@", userInfo);
    //把icon上的标记数字设置为0,
    application.applicationIconBadgeNumber = 0;
    NSString *beaconId = [[userInfo objectForKey:@"aps"] objectForKey:@"beaconId"];
    
    PushViewController *pushViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"PushViewController"];
    [pushViewController getGoodsInfoListWithBeaconId:beaconId];
    [self.window.rootViewController presentViewController:pushViewController animated:YES completion:nil];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
    NSLog(@"Failed to get token, error: %@", error);
}

#pragma mark -
#pragma mark OPEN URL

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    if ([url.scheme isEqualToString:@"tencent101098942"]) {
        if (YES == [TencentOAuth CanHandleOpenURL:url])
        {
            //UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Where from" message:url.description delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            //[alertView show];
            return [TencentOAuth HandleOpenURL:url];
        }
    }else if ([url.scheme isEqualToString:@"wb4094479862"]){
        return [WeiboSDK handleOpenURL:url delegate:self];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    if ([url.scheme isEqualToString:@"tencent101098942"]) {
        
        if (YES == [TencentOAuth CanHandleOpenURL:url])
        {
            //UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Where from" message:url.description delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
            //[alertView show];
            return [TencentOAuth HandleOpenURL:url];
        }
    }else if ([url.scheme isEqualToString:@"wb4094479862"]){
        return [WeiboSDK handleOpenURL:url delegate:self];
    }
    
    return YES;
}

#pragma mark -
#pragma mark WeiboSDKDelegate

- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
    if ([request isKindOfClass:WBProvideMessageForWeiboRequest.class])
    {
        
    }
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class])
    {
        NSString *title = @"发送结果";
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode, response.userInfo, response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([response isKindOfClass:WBAuthorizeResponse.class])
    {
        /*
        NSString *title = @"认证结果";
        NSString *message = [NSString stringWithFormat:@"响应状态: %d\nresponse.userId: %@\nresponse.accessToken: %@\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode,[(WBAuthorizeResponse *)response userID], [(WBAuthorizeResponse *)response accessToken], response.userInfo, response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
         [alert show];
        */
        if (response.statusCode == 0) {
            [[DataModel shareInstance]setSinaWeibo_AccessToken:[(WBAuthorizeResponse *)response accessToken] userId:[(WBAuthorizeResponse *)response userID]];
            
            PhoneLoginViewController *loginViewController = (PhoneLoginViewController*)[response.requestUserInfo objectForKey:@"SSO_From"];
            [loginViewController sinaWeiboLogin];
        }
    }
}


@end
