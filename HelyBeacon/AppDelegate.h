//
//  AppDelegate.h
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HelyEngine.h"

#import "QQEngine.h"
#import "SinaEngine.h"

#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) HelyEngine *helyEngine;
@property (strong, nonatomic) QQEngine *qqEngine;
@property (strong, nonatomic) SinaEngine *sinaEngine;

@end
