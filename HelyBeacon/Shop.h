//
//  Shop.h
//  HelyBeacon
//
//  Created by developer on 14-4-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Shop : NSObject
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *shopName;
@property (nonatomic, strong) NSURL *imageUrl;
@property (nonatomic, strong) NSString *shopDescription;
@property (nonatomic, assign) int favorNum;
@property (nonatomic, strong) NSArray *goods;
@property (nonatomic, strong) NSString *shopAddress;
@property (nonatomic, strong) NSString *shopTel;
@property (nonatomic, assign) float latitude;
@property (nonatomic, assign) float longitude;

- (void)updateWithDic:(NSDictionary*)dic;

@end
