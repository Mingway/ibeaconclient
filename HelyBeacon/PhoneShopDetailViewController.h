//
//  PhoneShopDetailViewController.h
//  HelyBeacon
//
//  Created by developer on 14-4-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Shop;
@interface PhoneShopDetailViewController : UIViewController
@property (nonatomic, strong)Shop *shop;

@end
