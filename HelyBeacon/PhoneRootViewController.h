//
//  PhoneRootViewController.h
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneRootViewController : UIViewController
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
