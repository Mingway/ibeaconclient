//
//  MapViewController.m
//  HelyBeacon
//
//  Created by developer on 14-5-7.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MapViewController.h"
#import "CalloutMapAnnotationView.h"
#import "Shop.h"
#import "BasicMapAnnotation.h"
#import "CalloutMapAnnotation.h"

@import MapKit;

@interface MapViewController () <MKMapViewDelegate>{
    BOOL _isLocated;
}

@property (nonatomic, retain) CalloutMapAnnotation *calloutAnnotation;
@property (nonatomic, retain) MKAnnotationView *selectedAnnotationView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.mapView.userInteractionEnabled = YES;
    self.mapView.showsUserLocation = YES;
    self.mapView.zoomEnabled = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _isLocated = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    _isLocated = YES;
}

- (void)config{
    for (Shop *shop in self.shops) {
        CLLocationDegrees latitude = shop.latitude;
        CLLocationDegrees longitude = shop.longitude;
        
        BasicMapAnnotation *customAnnotation = [[BasicMapAnnotation alloc] initWithLatitude:latitude andLongitude:longitude];
        customAnnotation.shop = shop;
        [self.mapView addAnnotation:customAnnotation];
        [self.mapView addAnnotation:customAnnotation];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if (!_isLocated) {
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 5000, 5000);
        MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:viewRegion];
        [_mapView setRegion:adjustedRegion animated:YES];
        _isLocated = YES;
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
	if ([view.annotation isKindOfClass:[BasicMapAnnotation class]]) {
		if (self.calloutAnnotation == nil) {
			self.calloutAnnotation = [[CalloutMapAnnotation alloc] initWithLatitude:view.annotation.coordinate.latitude andLongitude:view.annotation.coordinate.longitude];
		} else {
			self.calloutAnnotation.latitude = view.annotation.coordinate.latitude;
			self.calloutAnnotation.longitude = view.annotation.coordinate.longitude;
		}
        self.calloutAnnotation.shop = [(BasicMapAnnotation *)view.annotation shop];
		[self.mapView addAnnotation:self.calloutAnnotation];
		self.selectedAnnotationView = view;
	}else if([view isKindOfClass:[CalloutMapAnnotationView class]]){
        if ([_delegate respondsToSelector:@selector(didSelectedShop:)]) {
            [_delegate didSelectedShop:[(BasicMapAnnotation *)view.annotation shop]];
        }
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if (_calloutAnnotation&& ![view isKindOfClass:[CalloutMapAnnotationView class]])
    {
        if (_calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            _calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude)
        {
            CalloutMapAnnotation *oldAnnotation = self.calloutAnnotation; //saving it to be removed from the map later
            self.calloutAnnotation = nil; //setting to nil to know that we aren't showing a callout anymore
            dispatch_async(dispatch_get_main_queue(), ^{
                [mapView removeAnnotation:oldAnnotation]; //removing the annotation a bit later
            });
        }
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
	if (annotation == self.calloutAnnotation) {
		CalloutMapAnnotationView *calloutMapAnnotationView = (CalloutMapAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CalloutAnnotation"];
		if (!calloutMapAnnotationView) {
			calloutMapAnnotationView = [[CalloutMapAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CalloutAnnotation"];
		}

        [calloutMapAnnotationView setShopInfo:self.calloutAnnotation.shop];
		return calloutMapAnnotationView;
	} else if ([annotation isKindOfClass:[BasicMapAnnotation class]]) {
		MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomAnnotation"];
		annotationView.canShowCallout = NO;
		annotationView.image = [UIImage imageNamed:@"pin"];;
		return annotationView;
	}
	
	return nil;
}

- (void)viewDidUnload {
	self.mapView = nil;
}

@end
