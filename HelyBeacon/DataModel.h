//
//  DataModel.h
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TencentOpenAPI/TencentOAuth.h>

typedef NS_ENUM(int, LoginType) {
    LoginTypeNone = 1,
    LoginTypeRegister,
    LoginTypeQQ,
    LoginTypeWX,
    LoginTypeTCWeibo,
    LoginTypeSinaWeibo
};

@class QQUserInfo;
@class SinaWeiboUserInfo;
@interface DataModel : NSObject

@property (nonatomic, assign) BOOL isLogin;
@property (nonatomic, assign) LoginType loginType;
@property (nonatomic, strong) NSString *QQ_AccessToken;
@property (nonatomic, strong) NSString *QQ_OpenId;
@property (nonatomic, strong) QQUserInfo *QQ_UserInfo;

@property (nonatomic, strong) NSString *Sina_AccessToken;
@property (nonatomic, strong) NSString *Sina_UserId;
@property (nonatomic, strong) SinaWeiboUserInfo *Sina_UserInfo;


+ (instancetype)shareInstance;

//qq
- (void)setQQ_AccessToken:(NSString*)accessToken openId:(NSString*)openId;
- (void)qqLogoff;

//sina weibo
- (void)setSinaWeibo_AccessToken:(NSString*)accessToken userId:(NSString*)userId;
- (void)sinaLogoff;

@end
