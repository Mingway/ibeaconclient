//
//  GoodsInfoCell.m
//  HelyBeacon
//
//  Created by developer on 14-5-15.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "GoodsInfoCell.h"
#import "Goods.h"

@interface GoodsInfoCell (){
    Goods *_goods;
}
@property (weak, nonatomic) IBOutlet UILabel *goodsNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodsDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodsPriceLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
- (IBAction)favor:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *favorButton;


@end

@implementation GoodsInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setGoodsInfo:(Goods *)goods{
    _goods = goods;
    
    if ([self isfavor]) {
        [self.favorButton setSelected:YES];
    }else{
        [self.favorButton setSelected:NO];
    }
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:15.f]};
    CGSize labelMaxSize = CGSizeMake(280, 1000);
    CGRect labelBounds = [goods.goodsName boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    self.goodsNameLabel.text = goods.goodsName;
    [self.goodsNameLabel setFrame:CGRectMake(20, 60, labelBounds.size.width, labelBounds.size.height)];
    
    attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
    labelBounds = [goods.goodsDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    self.goodsDescriptionLabel.text = goods.goodsDescription;
    [self.goodsDescriptionLabel setFrame:CGRectMake(21, CGRectGetMaxY(self.goodsNameLabel.frame) + 5, labelBounds.size.width, labelBounds.size.height)];
    
    self.goodsPriceLabel.text = [NSString stringWithFormat:@"%.2f",[goods.price floatValue]];
    
    [self.bottomView setFrame:CGRectMake(0, CGRectGetMaxY(self.goodsDescriptionLabel.frame) + 10, 320, 30)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)favor:(UIButton *)sender {
    if (sender.isSelected) {
        [self removeFavorFromList];
        [sender setSelected:NO];
    }else{
        [self addFavorToList];
        [sender setSelected:YES];
    }
}

- (void)removeFavorFromList{
    NSMutableArray *favors = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getFilePath]];
    if (!favors) {
        favors = [NSMutableArray array];
    }
    for (Goods *goods in favors) {
        if ([goods.goodsId isEqualToString:_goods.goodsId]) {
            [favors removeObject:goods];
            break;
        }
    }
    if (favors && [favors count]>0) {
        [NSKeyedArchiver archiveRootObject:favors toFile:[self getFilePath]];
    }else {
        //删除归档文件
        NSFileManager *defaultManager = [NSFileManager defaultManager];
        if ([defaultManager isDeletableFileAtPath:[self getFilePath]]) {
            [defaultManager removeItemAtPath:[self getFilePath] error:nil];
        }
    }
}

- (void)addFavorToList{
    NSMutableArray *favors = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getFilePath]];
    if (!favors) {
        favors = [NSMutableArray array];
    }
    [favors addObject:_goods];
    if (favors && [favors count]>0) {
        [NSKeyedArchiver archiveRootObject:favors toFile:[self getFilePath]];
    }else {
        //删除归档文件
        NSFileManager *defaultManager = [NSFileManager defaultManager];
        if ([defaultManager isDeletableFileAtPath:[self getFilePath]]) {
            [defaultManager removeItemAtPath:[self getFilePath] error:nil];
        }
    }
}

- (BOOL)isfavor{
    NSMutableArray *favors = [NSKeyedUnarchiver unarchiveObjectWithFile:[self getFilePath]];
    if (!favors) {
        favors = [NSMutableArray array];
    }
    
    BOOL isFovr = NO;
    for (Goods *goods in favors) {
        if ([goods.goodsId isEqualToString:_goods.goodsId]) {
            isFovr = YES;
            break;
        }
    }
    return isFovr;
}

- (NSString*)getFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) ;
    NSString *documentD = [paths objectAtIndex:0];
    NSString *configFile = [documentD stringByAppendingPathComponent:@"favorList.plist"];
    return configFile;
}

@end
