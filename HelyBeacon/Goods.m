//
//  Goods.m
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "Goods.h"

const  double EPSILON = 0.000001;

@implementation Goods

//对属性编码，归档的时候会调用
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.goodsId forKey:@"goodsId"];
    [aCoder encodeObject:self.thumbnailUrl forKey:@"thumbnailUrl"];
    [aCoder encodeFloat:self.thumbnailWidth forKey:@"thumbnailWidth"];
    [aCoder encodeFloat:self.thumbnailHeight forKey:@"thumbnailHeight"];
    [aCoder encodeObject:self.goodsDescription forKey:@"description"];
    [aCoder encodeObject:self.price forKey:@"price"];
    [aCoder encodeInt:self.favorNum forKey:@"favorNum"];
    [aCoder encodeObject:self.imageUrl forKey:@"imageUrl"];
    [aCoder encodeObject:self.longDescription forKey:@"longDescription"];
    [aCoder encodeFloat:self.imageWidth forKey:@"imageWidth"];
    [aCoder encodeFloat:self.imageHeight forKey:@"imageHeight"];
    [aCoder encodeObject:self.goodsName forKey:@"goodsName"];
    [aCoder encodeObject:self.shopId forKey:@"shopId"];
}

//对属性解码，解归档调用
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self != nil) {
        self.goodsId = [aDecoder decodeObjectForKey:@"goodsId"];
        self.thumbnailUrl = [aDecoder decodeObjectForKey:@"thumbnailUrl"];
        self.thumbnailWidth = [aDecoder decodeFloatForKey:@"thumbnailWidth"];
        self.thumbnailHeight = [aDecoder decodeFloatForKey:@"thumbnailHeight"];
        self.goodsDescription = [aDecoder decodeObjectForKey:@"description"];
        self.price = [aDecoder decodeObjectForKey:@"price"];
        self.favorNum = [aDecoder decodeIntForKey:@"favorNum"];
        self.imageUrl = [aDecoder decodeObjectForKey:@"imageUrl"];
        self.longDescription = [aDecoder decodeObjectForKey:@"longDescription"];
        self.imageWidth = [aDecoder decodeFloatForKey:@"imageWidth"];
        self.imageHeight = [aDecoder decodeFloatForKey:@"imagehHeight"];
        self.goodsName = [aDecoder decodeObjectForKey:@"goodsName"];
        self.shopId = [aDecoder decodeObjectForKey:@"shopId"];
    }
    return self;
}

- (void)updateWithDic:(NSDictionary*)dic
{
    if ([dic objectForKey:@"goodsId"] != [NSNull null]) {
        self.goodsId = [dic objectForKey:@"goodsId"];
    }
    if ([dic objectForKey:@"thumbnailUrl"] != [NSNull null]) {
        self.thumbnailUrl = [NSURL URLWithString:[dic objectForKey:@"thumbnailUrl"]];
    }
    if ([dic objectForKey:@"thumbnailWidth"] != [NSNull null]) {
        self.thumbnailWidth = [[dic objectForKey:@"thumbnailWidth"] floatValue];
    }
    if ([dic objectForKey:@"thumbnailHeight"] != [NSNull null]) {
        self.thumbnailHeight = [[dic objectForKey:@"thumbnailHeight"] floatValue];
    }
    if ([dic objectForKey:@"description"] != [NSNull null]) {
        self.goodsDescription = [dic objectForKey:@"description"];
    }
    if ([dic objectForKey:@"price"] != [NSNull null]) {
        self.price = [dic objectForKey:@"price"];
    }
    if ([dic objectForKey:@"favorNum"] != [NSNull null]) {
        self.favorNum = [[dic objectForKey:@"favorNum"]intValue];
    }
    if ([dic objectForKey:@"imageUrl"] != [NSNull null]) {
        self.imageUrl = [NSURL URLWithString:[dic objectForKey:@"imageUrl"]];
    }
    if ([dic objectForKey:@"imageWidth"] != [NSNull null]) {
        self.imageWidth = [[dic objectForKey:@"imageWidth"] floatValue];
    }
    if ([dic objectForKey:@"imageHeight"] != [NSNull null]) {
        self.imageHeight = [[dic objectForKey:@"imageHeight"] floatValue];
    }
    if ([dic objectForKey:@"longDescription"] != [NSNull null]) {
        self.longDescription = [dic objectForKey:@"longDescription"];
    }
    if ([dic objectForKey:@"goodsName"] != [NSNull null]) {
        self.goodsName = [dic objectForKey:@"goodsName"];
    }
    if ([dic objectForKey:@"shopId"] != [NSNull null]) {
        self.shopId = [dic objectForKey:@"shopId"];
    }
    if (fabs(self.thumbnailHeight) < EPSILON) {
        self.thumbnailHeight = 150;
    }
    if (fabs(self.thumbnailWidth) < EPSILON) {
        self.thumbnailWidth = 150;
    }
    if (fabs(self.imageHeight) < EPSILON) {
        self.imageHeight = 150;
    }
    if (fabs(self.imageWidth) < EPSILON) {
        self.imageWidth = 150;
    }
}

@end
