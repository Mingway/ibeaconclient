//
//  ShopCell.m
//  HelyBeacon
//
//  Created by developer on 14-4-23.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "ShopCell.h"
#import "Shop.h"
#import "MKNetworkKit.h"

@import QuartzCore;

@interface ShopCell (){
    Shop *_shop;
}

@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
@property (weak, nonatomic) IBOutlet UIImageView *shopImageView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation ShopCell

//===========================================================
// + (BOOL)automaticallyNotifiesObserversForKey:
//
//===========================================================
+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImage"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}

-(void) prepareForReuse {
    
    self.shopImageView.image = nil;
    [self.imageLoadingOperation cancel];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
}

- (void)configWithShop:(Shop*)shop{
    _shop = shop;
    [self.shopImageView setImageFromURL:shop.imageUrl
                            placeHolderImage:[UIImage imageNamed:@"placeholder"]];

    self.nameLabel.text = shop.shopName;
    // 50 162
    NSDictionary *attribute = @{NSFontAttributeName: self.addressLabel.font};
    CGSize labelMaxSize = CGSizeMake(162, 30);
    CGRect labelBounds = [shop.shopAddress boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    [self.addressLabel setFrame:CGRectMake(CGRectGetMinX(self.addressLabel.frame), CGRectGetMinY(self.addressLabel.frame), labelBounds.size.width, labelBounds.size.height)];
    self.addressLabel.text = shop.shopAddress;
    
    
    [self.shopImageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
    self.shopImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.shopImageView.clipsToBounds = YES;
    self.shopImageView.backgroundColor = [UIColor whiteColor];
    self.shopImageView.layer.borderColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1.0].CGColor;
    self.shopImageView.layer.borderWidth = 1;
    self.shopImageView.layer.shadowOpacity = 0.3f;
    self.shopImageView.layer.shadowRadius = 1.5f;
    self.shopImageView.layer.shadowOffset = CGSizeZero;
    self.shopImageView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    self.shopImageView.layer.cornerRadius = 3;
}

@end
