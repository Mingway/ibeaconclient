//
//  SinaEngine.m
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-21.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "SinaEngine.h"
#import "DataModel.h"
#import "CommonConstants.h"

#import "SinaWeiboUserInfo.h"

@implementation SinaEngine

-(id) initWithDefaultSettings {
    
    if(self = [super initWithHostName:@"api.weibo.com" customHeaderFields:nil]) {
        
    }
    return self;
}

-(void) userInfoForCompletionHandler:(SinaUserInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    if ([DataModel shareInstance].Sina_AccessToken && [DataModel shareInstance].Sina_UserId) {
        MKNetworkOperation *op = [self operationWithPath:@"users/show.json" params:@{ @"access_token" : [DataModel shareInstance].Sina_AccessToken , @"uid" : [DataModel shareInstance].Sina_UserId} httpMethod:@"GET" ssl:YES];
        //    在请求中添加证书
        op.clientCertificate = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"client.p12"];
        op.clientCertificatePassword = @"test";
        //   当服务器端证书不合法时是否继续访问
        op.shouldContinueWithInvalidCertificate=YES;
        [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
            
            [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
                NSLog(@"%@",jsonObject);
                if ([[jsonObject objectForKey:@"ret"] intValue] == 0) {
                    SinaWeiboUserInfo *userInfo = [[SinaWeiboUserInfo alloc]init];
                    [userInfo updateWithDic:jsonObject];
                    responseBlock(YES,userInfo);
                }else{
                    responseBlock(NO,nil);
                }
            }];
            
        } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
            
            errorBlock(error);
        }];
        
        [self enqueueOperation:op];
    }
}

@end
