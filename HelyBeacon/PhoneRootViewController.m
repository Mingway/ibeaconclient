//
//  PhoneRootViewController.m
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneRootViewController.h"
#import "WaterFallFlowLayout.h"
#import "WaterFallFlowCell.h"
#import "Goods.h"
#import "AppDelegate.h"
#import "MJRefresh.h"
#import "PhoneGoodsDetailViewController.h"

#define CELL_WIDTH 150
#define CELL_IDENTIFIER @"WaterfallCell"

@interface PhoneRootViewController ()<UICollectionViewDataSource, WaterFallFlowLayout, MJRefreshBaseViewDelegate>{
    CGFloat _contentOffsetY;
    CGFloat _oldContentOffsetY;
    CGFloat _newContentOffsetY;
    
    MJRefreshHeaderView *_header;
    MJRefreshFooterView *_footer;
    int _totalCount;
}
@property (nonatomic, strong) NSMutableArray *goods;

@end

@implementation PhoneRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = self.collectionView;
    header.delegate = self;
    [header beginRefreshing];
    _header = header;
    
    MJRefreshFooterView *footer = [MJRefreshFooterView footer];
    footer.scrollView = self.collectionView;
    footer.delegate = self;
    _footer = footer;
    
    [self.collectionView registerClass:[WaterFallFlowCell class] forCellWithReuseIdentifier:CELL_IDENTIFIER];
    _totalCount = 0;
    [self collectionViewAddLayout];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
        [self.collectionView setFrame:CGRectMake(0, 20, 320, 460)];
    }
}

- (void)getDataIsRefresh:(BOOL)isRefresh{
    int pageIndex = 0;
    int pageSize = 10;
    
    if (!isRefresh) {
        if (self.goods.count % pageSize == 0) {
            pageIndex = (int)self.goods.count / pageSize;
        }else{
            pageIndex = (int)self.goods.count / pageSize + 1;
        }
    }
    
    [ApplicationDelegate.helyEngine goodsListForPageIndex:pageIndex pageSize:pageSize completionHandler:^(BOOL isSuccess ,NSArray *goods, int totalCount) {
        if (isRefresh) {
            [_header endRefreshing];
        }else{
            [_footer endRefreshing];
        }
        if (isSuccess) {
            _totalCount = totalCount;
            if (isRefresh) {
                [self.goods removeAllObjects];
            }
            
            if (!self.goods) {
                self.goods = [NSMutableArray array];
            }
            [self.goods addObjectsFromArray:goods];
            [self.collectionView reloadData];
        }else{
            //errCode != 0
        }
    } errorHandler:^(NSError *error) {
        if (isRefresh) {
            [_header endRefreshing];
        }else{
            [_footer endRefreshing];
        }
    }];
}

- (void)refresh{
    [self getDataIsRefresh:YES];
}

- (void)loadMore{
    [self getDataIsRefresh:NO];
}

- (void)collectionViewAddLayout{
    WaterFallFlowLayout *layout = [[WaterFallFlowLayout alloc] init];
    
    layout.sectionInset = UIEdgeInsetsMake(0, 8, 8, 8);
    layout.delegate = self;
    self.collectionView.collectionViewLayout = layout;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self updateLayout];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
                                            duration:duration];
    [self updateLayout];
}

- (void)updateLayout{
    WaterFallFlowLayout *layout =
    (WaterFallFlowLayout *)self.collectionView.collectionViewLayout;
    layout.columnCount = self.collectionView.bounds.size.width / CELL_WIDTH;
    layout.itemWidth = CELL_WIDTH;
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.goods ? self.goods.count : 0;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    WaterFallFlowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    [cell configCellWithGoods:self.goods[indexPath.item]];
    
    if ((indexPath.item == self.goods.count - 1 && self.goods.count < _totalCount) && !_footer.refreshing) {
        [self loadMore];
    }
    
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PhoneGoodsDetailViewController *detailViewController = [[UIApplication sharedApplication].keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"PhoneGoodsDetailViewController"];
    [detailViewController setGoods:self.goods[indexPath.item]];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - UICollectionViewWaterfallLayoutDelegate

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(WaterFallFlowLayout *)collectionViewLayout
 heightForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [self caculateCellHeightWithGoods:self.goods[indexPath.item]];
}

- (float)caculateCellHeightWithGoods:(Goods *)goods{
    float height = 0.0f;
    //缩略图片高度
    height += CELL_WIDTH * goods.thumbnailHeight /goods.thumbnailWidth;
    //图片和描述之间的高度
    height += 10;
    //描述高度
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.0f]};
    CGSize labelMaxSize = CGSizeMake(CELL_WIDTH - 10, 500);
    CGRect labelBounds = [goods.goodsDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
    height +=  labelBounds.size.height;
    //描述与价格之间的高度
    height += 10;
    //价格高度
    height += 20;
    //价格与底部之间的高度
    height += 5;
    
    return height;
}

#pragma mark -
#pragma mark UIScrollViewDelegate
//开始拖拽视图
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _contentOffsetY = scrollView.contentOffset.y;
}

// 滚动时调用此方法(手指离开屏幕后)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    _newContentOffsetY = scrollView.contentOffset.y;
    if (_newContentOffsetY > _oldContentOffsetY && _oldContentOffsetY > _contentOffsetY) {
        // 向上滚动
    } else if (_newContentOffsetY < _oldContentOffsetY && _oldContentOffsetY < _contentOffsetY) {
        // 向下滚动
    } else {
        // 拖动
    }

    if (scrollView.dragging) {
        // 拖拽
        if ((scrollView.contentOffset.y - _contentOffsetY) > 5.0f) {
            // 向上拖拽
            self.tabBarController.tabBar.hidden = YES;
        } else if ((_contentOffsetY - scrollView.contentOffset.y) > 5.0f) {
            // 向下拖拽
            self.tabBarController.tabBar.hidden = NO;
        } else {
        }
    }
}

// 完成拖拽(滚动停止时调用此方法，手指离开屏幕前)
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    _oldContentOffsetY = scrollView.contentOffset.y;
}

#pragma mark -
#pragma mark MJRefreshDelegate

- (void)refreshViewBeginRefreshing:(MJRefreshBaseView *)refreshView{
    if ([refreshView isKindOfClass:[MJRefreshHeaderView class]]) {
        [self refresh];
    }else{
        [self loadMore];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
