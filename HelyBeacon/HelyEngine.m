//
//  HelyEngine.m
//  HelyBeacon
//
//  Created by Mingway Shi on 14-5-18.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "HelyEngine.h"

@implementation HelyEngine

#pragma mark -
#pragma mark GoodsInfo&&ShopInfo

-(void) goodsListForPageIndex:(int) pageIndex pageSize:(int)pageSize completionHandler:(HelyGoodsListResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/client/interfaceForFeatured.html" params:@{@"pageIndex" : @(pageIndex), @"pageSize" : @(pageSize)} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            NSLog(@"goods list =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                NSMutableArray *goods = [NSMutableArray array];
                for (NSDictionary *dic in [jsonObject objectForKey:@"retValue"]) {
                    Goods *goodsInfo = [[Goods alloc]init];
                    [goodsInfo updateWithDic:dic];
                    [goods addObject:goodsInfo];
                }
                int totalCount = [[jsonObject objectForKey:@"totalCount"] intValue];
                responseBlock(YES,goods,totalCount);
            }else{
                responseBlock(NO,nil,0);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) goodsInfoForGoodsId:(NSString *)goodsId completionHandler:(HelyGoodsInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/client/interfaceForGoodsInfo.html" params:@{@"goodsId" : goodsId} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            NSLog(@"goods info =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                responseBlock(YES,[jsonObject objectForKey:@"retValue"]);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) ShopListForLatitude:(float)latitude longitude:(float)longitude completionHandler:(HelyShopListResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/client/interfaceForNear.html" params:@{@"latitude" : @(latitude) , @"longitude" : @(longitude)} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            NSLog(@"shop list =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"]intValue] == 0) {
                NSMutableArray *shops = [NSMutableArray array];
                for (NSDictionary *dic in [jsonObject objectForKey:@"retValue"]) {
                    Shop *shop = [[Shop alloc]init];
                    [shop updateWithDic:dic];
                    [shops addObject:shop];
                }
                responseBlock(YES,shops);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) shopInfoForShopId:(NSString *)shopId completionHandler:(HelyShopInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/client/interfaceForShopInfo.html" params:@{@"shopId" : shopId} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            NSLog(@"shop info =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"]intValue] == 0) {
                Shop *shop = [[Shop alloc]init];
                [shop updateWithDic:[jsonObject objectForKey:@"retValue"]];
                responseBlock(YES,shop);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) getPushForUUID:(NSString*)uuid major:(NSNumber*)major minor:(NSNumber*)minor completionHandler:(HelyPushResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/client/getPushMessage.html" params:@{@"uuid" : uuid , @"major" : major , @"minor" : minor , @"deviceToken" : deviceToken } httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            NSLog(@" result =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"]intValue] == 0) {
                responseBlock(YES);
            }else{
                responseBlock(NO);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) goodsListForBeaconId:(NSString*)beaconId completionHandler:(HelyGoodsListForBeaconResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/client/goodsListForBeacon.html" params:@{@"beaconId" : beaconId} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            NSLog(@"goods list =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                NSMutableArray *goods = [NSMutableArray array];
                for (NSDictionary *dic in [jsonObject objectForKey:@"retValue"]) {
                    Goods *goodsInfo = [[Goods alloc]init];
                    [goodsInfo updateWithDic:dic];
                    [goods addObject:goodsInfo];
                }
                responseBlock(YES,goods);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

#pragma mark -
#pragma mark CacheDir

-(NSString*) cacheDirectoryName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *cacheDirectoryName = [documentsDirectory stringByAppendingPathComponent:@"HelyImages"];
    return cacheDirectoryName;
}

@end
