//
//  PhoneLoginViewController.h
//  HelyBeacon
//
//  Created by developer on 14-4-24.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PhoneLoginViewControllerDelegate <NSObject>

@optional
- (void)didLoginQQ;
- (void)didLoginSinaWeibo;

@end

@interface PhoneLoginViewController : UIViewController

@property (nonatomic) id<PhoneLoginViewControllerDelegate> delegate;

- (void)sinaWeiboLogin;

@end
