//
//  Utility.h
//  HelyBeacon
//
//  Created by developer on 14-4-17.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Shop;
@class Goods;
@interface Utility : NSObject

+ (CGFloat)caculateCellHeightWithShopInfo:(Shop *)shop;
+ (CGFloat)caculateCellHeightWithGoodsInfo:(Goods *)goods;

@end
